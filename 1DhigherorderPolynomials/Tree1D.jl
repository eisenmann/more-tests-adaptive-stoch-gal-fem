# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


const DyadicIndex1D = Tuple{Int64,Int64}
const VertexIndex1D = Tuple{Int64,Int64}

const DyadicIndexType = DyadicIndex1D
const VertexIndexType = VertexIndex1D


function StaticRoots()
    return [(0,0)]
end

function j(tile::DyadicIndex1D)
    return tile[1]
end
function k(tile::DyadicIndex1D)
    return tile[2]
end

#return the children using propper tree order
function children(idx::DyadicIndex1D, l::Integer = 1) 
    if l == 0
        return (idx)
    else
        return [ (j(idx) + l, 2^l * k(idx) + offset ) for offset in 0:2^l-1]
    end
end

childrenTuple(idx::DyadicIndex1D) = ((idx[1]+1,idx[2]*2),(idx[1]+1,idx[2]*2+1))

function ancestor(idx::DyadicIndex1D,l::Integer=1)
    @assert j(idx) >= l
    return (j(idx) - l, k(idx) .÷ 2^l)
end

area(tile::DyadicIndex1D) = 0.5^tile[1]


#returns lower left corner and upper right corner
function getDomain(idx::DyadicIndex1D)
    return (exp2(-j(idx)) * k(idx), exp2(-j(idx)) * (k(idx) + 1) )
end

coords(vertex::VertexIndex1D) = vertex[2] * 0.5^vertex[1]

function bary(tile::DyadicIndex1D, vertex::VertexIndex1D) 
    x = (vertex[2] - tile[2]*2^(vertex[1]-tile[1]) )*0.5^(vertex[1] -tile[1])
    return 1-x, x
end

function vertexes(idx::DyadicIndex1D)
    return (idx,(idx[1],idx[2]+1))
end


function getVertexParents(v::DyadicIndex1D)
    return ((v[1]-1,v[2]÷2), (v[1]-1,v[2]÷2+1) )
end

function getBnd(idx::DyadicIndex1D)
    return getDomain(idx)
end

function normal(idx::DyadicIndex1D, a::Float64)
    return a==getDomain(idx)[1] ? -1.0 : 1.0
end


function common_ancestor(idx1::DyadicIndex1D,idx2::DyadicIndex1D)
    l = min(j(idx1),j(idx2))
    different = ceil(Int,log2( xor( k(ancestor_abs(idx1,l) ) ,k( ancestor_abs(idx2,l)) )+1 ) )
    return ancestor_abs(idx1,l-different)
end

function geometric_next(idx::DyadicIndex1D)
    return idx .+ (0,1)
end

function simplifyVertex(node::VertexIndex1D)
    while node[2] % 2 == 0 && node[1]>0
        node = (node[1]-1, node[2] ÷ 2)
    end
    return node
end

function simpVertex(tile::DyadicIndex1D)
    [simplifyVertex( (tile[1],tile[2]) ), simplifyVertex( (tile[1],tile[2]+1) ) ] 
end
function onBoundary(node::VertexIndex1D)
    return node[2] == 0 || node[2] == 2^node[1]
end

function inDomain(v::VertexIndex1D)
    return !onBoundary(v)
end


#tree with inner nodes as keys and (a vector of) children as values - e.g. empty tree is only the root
#keys are inner nodes, leaves are the only nodes that are only children (of some key) and not
const treeDict = Dict{DyadicIndexType, Vector{DyadicIndexType}}
const piecewisePoly = Dict{DyadicIndexType,SVector{polydegree+1,Float64}}
const Tiling = Vector{DyadicIndexType}
const Mesh = Vector{DyadicIndexType} #Vector of sorted (left to right in tree) tiles, that discretise the entire domain
const SubMesh = Vector{DyadicIndexType} #Vector of sorted (left to right in tree) tiles, that discretise only part of the getDomain
const MeshDict = Matrix{Int64} # a dictionary for indexes column is the tile and row is the local index
const SmartMesh = Tuple{Mesh, MeshDict, Int64, treeDict, Dict{DyadicIndexType, Int64}} #Mesh, Index for DoFs, Fe Dim, tree for the mesh, map leaves -> index in mesh

#starting from here, the routines should not depend on the spatial dimension and work depending on a proper implementation 
# of routines like children, ancestor, 
function isRoot(idx::DyadicIndexType)
    return j(idx) == 0
end

function children_abs(idx::DyadicIndexType, l::Integer)
    #@assert (j(idx)≤l)
    return children(idx,l-j(idx))
end

function children(tiles::Tiling, l::Integer=1)
    return reduce(vcat, children(tile, l) for tile in tiles)
end

function ancestor_abs(idx::DyadicIndexType,l::Integer)
    #@assert (j(idx)≥l)
    return ancestor(idx,j(idx)-l)
end
#check intersection tiles and tilings as Bool
function intersect(tile1::DyadicIndexType, tile2::DyadicIndexType)
    level = min(j(tile1),j(tile2))
    return ancestor_abs(tile1,level) == ancestor_abs(tile2,level)
end
function intersect(tiles::Tiling, tile2::DyadicIndexType)
    ans = false
    for tile1 in tiles
        if intersect(tile1,tile2)
            ans = true
        end
    end
    return ans
end
function intersection(tile1::DyadicIndexType,tile2::DyadicIndexType)
    @assert intersect(tile1, tile2)
    return j(tile1) > j(tile2) ? tile1 : tile2
end

function strictlylessintree(tile1::DyadicIndexType, tile2::DyadicIndexType)
    anc1 = deepcopy(tile1)
    anc2 = deepcopy(tile2)
    if j(anc1) ≠ j(anc2)
        level = min(j(tile1),j(tile2))
        anc1 = ancestor_abs(tile1,level)
        anc2 = ancestor_abs(tile2,level)
    end
    siblings = j(anc1)==0 ? StaticRoots() : children(ancestor(anc1))
    while anc2 ∉ siblings
        anc1 = ancestor(anc1)
        anc2 = ancestor(anc2)
        siblings = j(anc1)==0 ? StaticRoots() : children(ancestor(anc1))
    end
    return indexin(anc1,siblings) < indexin(anc2,siblings)

end


function refine(tiling::Tiling, mark::Vector{Bool},l::Integer=1)
    if l>0
        len = (length(mark) - count(mark))+count(mark)*2^(l)
        refined = Tiling(undef,len)
        i = 0
        for (flag,idx) in zip(mark,tiling)
            if flag
                children_i = children(idx,l)
                len_i = length(children_i)
                refined[i+1:i+len_i] .= children_i
                i += len_i
            else
                i += 1
                refined[i] = idx
            end
        end
        return refined
    else
        return deepcopy(tiling)
    end
end


function refine(tiling::Tiling,l::Integer=1)
    return refine(tiling,[true for t in tiling],l)
end

#makes a tree from leaves, only for disjoint tiles
#1 dimensial optimized version
function makeTree(tiles::Tiling)
    	#first sort tiles into level buckets
  @timeit to "makeTree" begin
	levelTiles = Vector{Vector{DyadicIndexType}}()
	for tile in tiles
		while length(levelTiles) ≤ tile[1]
			push!(levelTiles,Vector{DyadicIndexType}())
		end
        a = tile 
        while  a[1]!=0  && (length(levelTiles[a[1]+1])== 0 || levelTiles[a[1]+1][end] != a )
		    push!(levelTiles[a[1]+1], a)
            a = ancestor(a)
        end
	end
    tree = treeDict()
	for l in length(levelTiles):-1:2
		for t in levelTiles[l]
            a = ancestor(t)
            if haskey(tree,a)
                if t ∉ tree[a]
                    push!(tree[a],t)
                end
            else
                tree[a] = [t]
            end
		end	
	end
  end
    return tree
end




#makes minimal tree from set of tiles and sums the values on children

function removeLeaves(tree::treeDict)
    return makeMinTree(collect(keys(tree)))
end




#get leaves as SubMesh from subtree starting at a node
function getLeaves(tree::treeDict, nodes::Vector{DyadicIndexType})
    # @assert reduce(&, [isRoot(node) || haskey(tree, ancestor(node)) for node in nodes] )
    stack = deepcopy(nodes)
    submesh = SubMesh()
    while length(stack) > 0 
        s = pop!(stack)
        if haskey(tree,s)
            append!(stack, reverse!(children(s)) )
        else
            push!(submesh,s)
        end
    end
    return submesh
end

function getLeaves(tree::treeDict, node::DyadicIndexType)
    stack = [ node ]
    submesh = SubMesh()
    while length(stack) > 0 
        s = pop!(stack)
        if haskey(tree,s)
            append!(stack, reverse!(children(s)) )
        else
            push!(submesh,s)
        end
    end
    return submesh
end

function findIntersection(tree::treeDict, node::DyadicIndexType)
    if isRoot(node) || haskey(tree, ancestor(node)) 
        return getLeaves(tree, node)
    else
        anc = deepcopy(node)
        while !(isRoot(anc) || haskey(tree, ancestor(anc)) )
            anc = ancestor(anc)
        end
        if haskey(tree, anc) && intersect(node,tree[anc])
            return [tree[anc]]
        else
            return [node][1:0]
        end
    end
end




#get mesh from tree
function getMesh(tree::treeDict)
    getLeaves(tree, (0,0) )
end

function getMesh(tiles::Tiling)
    getMesh( makeTree(tiles) )
end


function height(tree::treeDict)
    return max(0, maximum([v[1][1] for (k,v) in tree]))+1
end
#get levelwise submeshes from tree
function getSubMeshes(tree::treeDict)

    stack = StaticRoots()
    submeshes = Vector{SubMesh}(undef, height(tree))
    for i in 1:length(submeshes) 
        submeshes[i] = SubMesh()
    end
    while length(stack) > 0 
        s = pop!(stack)
        if haskey(tree,s)
            append!(stack, reverse!(children(s)) )
        end
        push!(submeshes[ s[1]+1 ],s)
    end
    return submeshes
end

function makeTree(pp::piecewisePoly)
    tiles = collect(keys(pp))
    return makeTree(tiles)
end




