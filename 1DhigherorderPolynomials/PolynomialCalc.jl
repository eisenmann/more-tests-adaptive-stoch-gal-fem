using FastGaussQuadrature, StaticArrays

nodesvec, weightsvec = gausslobatto(polydegree+1)
lobattoweights = SVector{polydegree+1}(w/2 for w in weightsvec)
lobattonodes = SVector{polydegree+1}((x+1)/2 for x in nodesvec)


function getInterpolWeights(x::SVector{polydegree+1, Float64})
    c = [1.0]
    N = length(x)
    for j in 2:N
        new = x[j] .- x[1:j-1]
        c .*= new
        push!(c, prod(.-new))
    end
    return c
end

lobattoInterpolWeights = SVector{polydegree+1, Float64}(getInterpolWeights(lobattonodes))


function eval(x::Float64, f::SVector{polydegree+1, Float64}, X::SVector{polydegree+1, Float64})
    for (fi,xi) in zip(f,X)
        if x ≈ xi
            return fi
        end
    end
    y = x .- X 
    return sum(f./(lobattoInterpolWeights.*y))/sum(1 ./(lobattoInterpolWeights.*y))
end

function eval(x::Float64, f::SVector{polydegree+1, Float64})
    for (fi,xi) in zip(f,lobattonodes)
        if x ≈ xi
            return fi
        end
    end
    y = x .- lobattonodes 
    return sum(f./(lobattoInterpolWeights.*y))/sum(1 ./(lobattoInterpolWeights.*y))
end

function diffMatrix(x::SVector{polydegree+1, Float64}) 
    N = length(x)
    X = x * ones(N)'
    dX = X - X'
    D  = (lobattoInterpolWeights*(1.0./lobattoInterpolWeights)')./(dX +I)
    d = D * ones(N)
    return (D - Diagonal(d))
end

lobattoDiffMatrix = diffMatrix(lobattonodes)

localStiffness = lobattoDiffMatrix'*Diagonal(lobattoweights)*lobattoDiffMatrix

unitvector(i,n) = SVector{n}((1:n) .== i)


localSmoother = lu(localStiffness + localStiffness[1,1]*Diagonal(unitvector(1,polydegree+1)) + localStiffness[end,end]*Diagonal(unitvector(polydegree+1,polydegree+1)))


localSmootherL = lu(localStiffness[2:end,2:end] + localStiffness[end,end]*Diagonal(unitvector(polydegree,polydegree)))

localSmootherR = lu(localStiffness[1:end-1,1:end-1] + localStiffness[1,1]*Diagonal(unitvector(1,polydegree)))

localSmootherC = lu(localStiffness[2:end-1,2:end-1])

