# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


# auxiliary definitions and routines
# for defining and handling random PDEs
using LinearAlgebra, SparseArrays


import Base: zeros, iterate, length, eltype, +
import LinearAlgebra: norm, dot

struct OperatorInfo
    c::Float64
    Capprox::Float64
    α::Float64
    μiter::Type # subtype of OperatorIterable
end


const FunctionCoeff = Vector{Vector{Float64}}
const DGfunction = Tuple{Mesh, Vector{SVector{polydegree+1,Float64}}}

include("Tree.jl")




function FunctionCoeff(n::Int64)
    return [ Float64[] for i = 1:n ]
end


function zeros(u::FunctionCoeff)
    return [ zeros(length(u[i])) for i in eachindex(u) ]
end

function zeros(meshes::Vector{Mesh} )
    return [ zeros(dimFE(meshDict(mesh))) for mesh in meshes ]
end

function setzero(u::FunctionCoeff)
    broadcast(x->(x.=0.0), u)
end

function sqrtbeta(k::Integer)
    return 1.0/sqrt(4.0 - 1.0/k^2)
end


abstract type OperatorIterable end

function scaleFactor(opinfo::OperatorInfo, level::Int64)
    if  level ≥ 0
        return opinfo.c*2^(-opinfo.α* level)
    else
        return 1.
    end
end


function suppHat1D(μ::Int64) 
    j = μ == 0 ? 0 : floor(Int64, log2(μ))
    k = μ == 0 ? 0 : μ - 2^j
    # dyadic tile of support, level of further subdivision:
    #return [DyadicIndex{1}(j,2^j - k - 1)], 1
    return [(j,k)], 1
end

struct IterHat1D <: OperatorIterable
    maxμ::Int64
    function IterHat1D(L::Int64)
        if L >= 0
            maxμ = 2^(L+1) - 1
        else
            maxμ = 0
        end
        return new(maxμ)
    end
end

function get1DlevelRange(L::Int64)
    if L == -1
        return 0:0
    end
    return 2^(L)  :   2^(L+1) - 1
end

# return tuple of first iterate and initial state
function Base.iterate(iter::IterHat1D)
    if iter.maxμ > 0
        return (1, 1)
    else
        return nothing
    end
end

# returns tuple of next iterate and next state
function Base.iterate(iter::IterHat1D, state::Int64)
    if state < iter.maxμ
        return (state+1, state+1)
    else
        return nothing
    end
end

Base.length(iter::IterHat1D) = iter.maxμ
Base.eltype(iter::IterHat1D) = Int64




# iterate over subdivision of a tile defined by a DyadicIndex{1}
const DyadicIndex1D = Tuple{Int64,Int64}

struct SupportTile1D
    lower::Float64
    upper::Float64
    tile::DyadicIndex1D
    index::Int64
end

struct SupportTilesIter1D
    tile::DyadicIndex1D
    imax::Int64

    function SupportTilesIter1D(d::DyadicIndex1D, subdivlevel::Int64)
        @assert subdivlevel >= 0
        return new(children(d,subdivlevel)[1], 2^subdivlevel)
    end
end

function Base.iterate(iter::SupportTilesIter1D)
    return ( SupportTile1D(getDomain(iter.tile)..., iter.tile, 1), 1)
end

function Base.iterate(iter::SupportTilesIter1D, state::Int64)
    if state < iter.imax
        nextTile = geometric_next(iter.tile)
        return ( SupportTile1D(getDomain(nextTile)..., nextTile, state+1), state+1)
    else
        return nothing
    end
end

Base.length(iter::SupportTilesIter1D) = iter.imax
Base.eltype(iter::SupportTilesIter1D) = SupportTile1D


# evaluate ψ_μ on a given support tile

function evalHat1D(t::SupportTile1D, x::Float64)
    if t.index == 1
        return (x - t.lower) / (t.upper - t.lower)
    elseif t.index == 2
        return (t.upper - x) / (t.upper - t.lower)
    else
        return 0.
    end
end

function evalHat1D(μ::Int64, x::Float64)
    dyadic, subdivlvl = suppHat1D(μ)
    siter = SupportTilesIter1D(dyadic[1], subdivlvl)
    ans = 0.0
    for t in siter
        if x ≤ t.upper && x≥t.lower
            ans = evalHat1D(t,x)
        end
    end
    return ans
end


function get1Dtop(μ::Int64)
    level = floor(Int64,log2(μ))+1
    beforethislevel = 2^(level-1)-1
    return (level, (μ-beforethislevel-1)*2+1)
end

function get1DtopFromTile(tile::DyadicIndex1D)
    for v in vertexes(tile)
        if v == simplifyVertex(v) &&  inDomain(v)
            return (v,)
        end
    end
    return 0:-1
end


function get1DtopInverse(idx::DyadicIndex1D)
    level = idx[1]
    beforethislevel = 2^(level-1)-1
    return beforethislevel + idx[2]÷2 + 1
end

#1D function, finetile should be finer than the tiles of top
function evalTopAtTile(top::GlobalFE1D, finetile::DyadicIndex1D)
    for fe_i in localRep(top)
        if intersect(fe_i[1], finetile)
            return locfeTilevalues(fe_i, finetile) 
        end
    end
end


function locfeTilevalues(locfe::Tuple{DyadicIndex1D, Int64}, finetile::DyadicIndex1D) 
    tile = locfe[1]
    x1 = (finetile[2] - tile[2]*2^(finetile[1]-tile[1]) )*0.5^(finetile[1] -tile[1])
    x2 = (finetile[2]+1 - tile[2]*2^(finetile[1]-tile[1]) )*0.5^(finetile[1] -tile[1])
    if locfe[2] == 1
        return x1 .+ lobattonodes.*(x2-x1)
    else
        return (1-x1) .+ lobattonodes.* (x1-x2)
    end
end

function applyHatLevel!(level::Int64, u::Vector{Float64},  PPs::Vector{piecewisePoly}, mesh::Mesh, meshdict::Matrix{Int64}, 
                    indices::Matrix{Int64} , coeffs::Vector{Float64} = ones(length(PPs)))
    #indices tells us where to add to
    if  level == -1
        idx = indices[1,1]
        c = coeffs[idx]
        #go through mesh
        for (i,tile) in enumerate(mesh)
            
            uloc = SVector{polydegree+1}(meshdict[j,i] == 0 ? 0.0 : u[meshdict[j,i]] for j in 1:polydegree+1)
            g = lobattoDiffMatrix * uloc *2^tile[1]
            if haskey(PPs[idx], tile)
                PPs[idx][tile] = PPs[idx][tile] .+ g
            else
                PPs[idx][tile] = g
            end
        end
    else
        beforethislevel = 2^(level)-1
        for (i,tile) in enumerate(mesh)
            if tile[1] ≥ level + 1
                coarsetile = (level+1, tile[2]÷ 2^(tile[1] - level-1) )
                for top in get1DtopFromTile(coarsetile)
                    μ = get1DtopInverse(top) 
                    @views idx1, idx2 = indices[:, μ - beforethislevel]
                    if idx1 != 0 || idx2 != 0
                        c1 = idx1 == 0 ? 0 : coeffs[idx1]
                        c2 = idx2 == 0 ? 0 : coeffs[idx2]
                        b = evalTopAtTile(top, tile)
                        g = lobattoDiffMatrix * localPoly(u, meshdict, i) *2^tile[1]
                        if idx1 != 0
                            if haskey(PPs[idx1], tile)
                                PPs[idx1][tile] = PPs[idx1][tile] .+ g.*b.*c1
                            else
                                PPs[idx1][tile] = g.*b.*c1
                            end
                        end
                        if idx2 != 0 
                            if haskey(PPs[idx2], tile)
                                PPs[idx2][tile] = PPs[idx2][tile] .+ g.*b.*c2
                            else
                                PPs[idx2][tile] = g.*b.*c2
                            end
                        end
                    end
                end
            else
                vals = lobattoDiffMatrix* localPoly(u, meshdict, i) * 2^tile[1]
                childs = children(tile,(level+1 - tile[1]))
                for kid in childs
                    for top in get1DtopFromTile(kid)
                        μ = get1DtopInverse(top)
                        @views idx1, idx2 = indices[:, μ - beforethislevel]
                        if idx1 != 0 || idx2 != 0
                            c1 = idx1 != 0  ? coeffs[idx1] : 0.0
                            c2 = idx2 != 0 ? coeffs[idx2] : 0.0
                            b = evalTopAtTile(top, kid)
                    
                            g = translateTileValues(tile,vals,kid)

                            if idx1 != 0
                                if haskey(PPs[idx1], kid)
                                    PPs[idx1][kid] = PPs[idx1][kid] .+ g.*b.*c1
                                else
                                    PPs[idx1][kid] = g.*b.*c1
                                end
                            end
                            if idx2 != 0 
                                if haskey(PPs[idx2], kid)
                                    PPs[idx2][kid] = PPs[idx2][kid] .+ g.*b.*c2
                                else
                                    PPs[idx2][kid] = g.*b.*c2
                                end
                            end
                        end
                    end
                end
            end
        end

    end
end



