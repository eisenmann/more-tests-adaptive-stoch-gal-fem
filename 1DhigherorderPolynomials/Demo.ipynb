{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3>Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients</h3>\n",
    "Released under MIT License, see <a href=\"https://git.rwth-aachen.de/bachmayr/asgfemmlrrc/-/blob/22af3f3c5089f492f83dbfcc4517f07492620236/README.md\">README.md</a><br/>\n",
    "Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our aim is to approximate the solution of the model problem\n",
    "$$\n",
    "    -\\partial_x \\cdot(a(x,y) \\partial_x u(x,y)) = f(x) \\quad\\text{for $x\\in (0,1)$, }\\\\\n",
    "    u(0,y) = u(1,y) = 0,\n",
    "$$\n",
    "where \n",
    "$$\n",
    "    a(x,y) = \\theta_0(x)  +  \\sum_\\mu y_\\mu\\theta_\\mu(x)\n",
    "$$\n",
    "is a multilevel representation of a random field.\n",
    "\n",
    "We first include the necessary files for approximating the solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "thresholding (generic function with 1 method)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "include(\"AdaptiveSolver.jl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `driver1D` takes as input $\\alpha$ representing the decay in the diffusion coefficient and `N` as a stopping criterium for the size $\\mathcal N(\\mathbb T)$. It computes an approximation to the solution of the model problem with constant right-hand side.\n",
    "\n",
    "The output `F, S, u, timed` contains the required polynomial multiindices in `F`, the family of partitions in `S`, the coefficients of the solution in `u` and data concerning the error, degrees of freedom, and computational time, as well as intermediate solutions in `timed`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1D: maxS=10 α=0.6666666666666666\n",
      "[(1, 0), (1, 1)]\n",
      "   [ entered solveGalerkinSystem: F 1, S 2 \n",
      "cgiter started with r: 0.3739181926769743\n",
      "cgiter used 4 iterations\n",
      "cgiter ended with r: 7.760168699348174e-16\n",
      "resApprox: α = 0.6666666666666666ζ = 0.3333333333333333η0 = 0.03333333333333333\n",
      "   [ entered resApprox: F 1, S 2, ζeff = 1.389e-01\n",
      "      η\t\trnorm\t\tFext\t\tSext\n",
      "     9.165e-03\t 3.333e-02\n",
      "     3.333e-02\t3.187e-16\t1\t\t2\n",
      "     9.165e-03\t 1.667e-02\n",
      "     1.667e-02\t3.187e-16\t1\t\t2\n",
      "     5.774e-03\t 8.333e-03\n",
      "     8.333e-03\t6.519e-03\t2\t\t4\n",
      "     3.637e-03\t 4.167e-03\n",
      "     4.167e-03\t6.611e-03\t4\t\t10\n",
      "     1.443e-03\t 2.083e-03\n",
      "     2.083e-03\t6.695e-03\t16\t\t66\n",
      "     9.093e-04\t 1.042e-03\n",
      "     1.042e-03\t6.707e-03\t32\t\t162\n",
      "   [ exiting resApprox: norm_res = 6.707e-03,  η = 9.093e-04\n",
      "Step 1\n",
      "Step: ε , sec, F , S}\n",
      "8.5742e-03 , 0 , 1, 2 \n",
      "   [ entered solveGalerkinSystem: F 2, S 6 \n",
      "cgiter started with r: 0.014834477692020781\n",
      "Warning maxiter reached in cgiter, with residual 0.0005888982301081127\n",
      "cgiter used 500 iterations\n",
      "cgiter ended with r: 0.0005888982301081127\n",
      "resApprox: α = 0.6666666666666666ζ = 0.3333333333333333η0 = 0.0010416666666666667\n",
      "   [ entered resApprox: F 2, S 6, ζeff = 1.389e-01\n",
      "      η\t\trnorm\t\tFext\t\tSext\n",
      "     9.094e-04\t 1.042e-03\n",
      "     1.042e-03\t3.435e-03\t63\t\t328\n",
      "     3.609e-04\t 5.208e-04\n",
      "     5.208e-04\t3.449e-03\t255\t\t1800\n",
      "   [ exiting resApprox: norm_res = 3.449e-03,  η = 3.609e-04\n",
      "Step 2\n",
      "Step: ε , sec, F , S}\n",
      "8.5742e-03 , 0 , 1, 2 \n",
      "4.3028e-03 , 4 , 2, 6 \n",
      "   [ entered solveGalerkinSystem: F 2, S 12 \n",
      "cgiter started with r: 0.5007332817379903\n",
      "Warning maxiter reached in cgiter, with residual 0.0007220900221709103\n",
      "cgiter used 500 iterations\n",
      "cgiter ended with r: 0.0007220900221709103\n",
      "resApprox: α = 0.6666666666666666ζ = 0.3333333333333333η0 = 0.0005208333333333333\n",
      "   [ entered resApprox: F 2, S 12, ζeff = 1.389e-01\n",
      "      η\t\trnorm\t\tFext\t\tSext\n",
      "     4.901e-04\t 5.208e-04\n",
      "     5.208e-04\t7.102e-02\t127\t\t802\n",
      "   [ exiting resApprox: norm_res = 7.102e-02,  η = 4.901e-04\n",
      "Step 3\n",
      "Step: ε , sec, F , S}\n",
      "8.5742e-03 , 0 , 1, 2 \n",
      "4.3028e-03 , 4 , 2, 6 \n",
      "8.1659e-02 , 6 , 2, 12 \n"
     ]
    }
   ],
   "source": [
    "N = 10\n",
    "α = 2/3\n",
    "F, S, u, timed  = driver1D(α,N);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "resApprox: α = 0.6666666666666666ζ = 0.3333333333333333η0 = 0.1\n",
      "   [ entered resApprox: F 1, S 2, ζeff = 1.389e-01\n",
      "      η\t\trnorm\t\tFext\t\tSext\n",
      "     9.165e-03\t 1.000e-01\n",
      "     1.000e-01\t2.199e-01\t1\t\t2\n",
      "   [ exiting resApprox: norm_res = 2.199e-01,  η = 9.165e-03\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "0.1875"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "opinfo = Opinfo(α)\n",
    "δ = 0.5\n",
    "ζ = δ/(1+δ)\n",
    "η0 = 0.1\n",
    "\n",
    "u = [[1/8, 0.03944128843789428 ,0.09375 ,0.12127299727639143 ,0.12127299727639143, 0.09375, 0.03944128843789428] ] \n",
    "\n",
    "Fext, applied_approx ,BPXcoeffs, rerrbound, rnorm, η0 = resApprox(opinfo, F, S, meshDict.(S), u, ζ, η0) \n",
    "\n",
    "\n",
    "BPXcoeffs[1][(1,1)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-0.0625"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "applied_approx[1]\n",
    "getBPXcoeffs1D(applied_approx[1])[(1,1)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4-element Vector{Pair{Tuple{Int64, Int64}, Float64}}:\n",
       " (1, 1) => -0.05937499999999999\n",
       " (2, 2) => -0.019887378220871665\n",
       " (2, 1) => -0.022097086912079587\n",
       " (2, 3) => -0.02209708691207959"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "collect(getBPXcoeffs1D(applied_approx[1],false))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5×4 Matrix{Int64}:\n",
       " 0  1   5   9\n",
       " 2  6  10  13\n",
       " 3  7  11  14\n",
       " 4  8  12  15\n",
       " 1  5   9   0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "15-element Vector{Float64}:\n",
       " 0.12031249999999967\n",
       " 0.020774740121473404\n",
       " 0.06015624999999985\n",
       " 0.09953775987852706\n",
       " 0.11875000000000006\n",
       " 0.12004269818024088\n",
       " 0.11953124999999984\n",
       " 0.11901980181975944\n",
       " 0.12031250000000039\n",
       " 0.11901980181975919\n",
       " 0.11953125000000017\n",
       " 0.12004269818024016\n",
       " 0.09953775987852642\n",
       " 0.060156250000000175\n",
       " 0.02077474012147304"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "display(meshDict(S[1]))\n",
    "u[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now load files for visualization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "MonteCarloEstimator (generic function with 1 method)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "include(\"PresentationTools.jl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A convergence plot can be shown with the function `conv_plot1d` with the additional information of `timed` and $\\alpha$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 109,
   "metadata": {},
   "outputs": [
    {
     "ename": "MethodError",
     "evalue": "MethodError: reducing over an empty collection is not allowed; consider supplying `init` to the reducer",
     "output_type": "error",
     "traceback": [
      "MethodError: reducing over an empty collection is not allowed; consider supplying `init` to the reducer\n",
      "\n",
      "Stacktrace:\n",
      "  [1] mapreduce_empty(#unused#::typeof(identity), op::Function, T::Type)\n",
      "    @ Base ./reduce.jl:367\n",
      "  [2] reduce_empty(op::Base.MappingRF{typeof(identity), typeof(min)}, #unused#::Type{Real})\n",
      "    @ Base ./reduce.jl:356\n",
      "  [3] reduce_empty_iter\n",
      "    @ ./reduce.jl:379 [inlined]\n",
      "  [4] mapreduce_empty_iter(f::Function, op::Function, itr::Vector{Real}, ItrEltype::Base.HasEltype)\n",
      "    @ Base ./reduce.jl:375\n",
      "  [5] _mapreduce(f::typeof(identity), op::typeof(min), #unused#::IndexLinear, A::Vector{Real})\n",
      "    @ Base ./reduce.jl:427\n",
      "  [6] _mapreduce_dim\n",
      "    @ ./reducedim.jl:365 [inlined]\n",
      "  [7] #mapreduce#800\n",
      "    @ ./reducedim.jl:357 [inlined]\n",
      "  [8] mapreduce\n",
      "    @ ./reducedim.jl:357 [inlined]\n",
      "  [9] #_minimum#822\n",
      "    @ ./reducedim.jl:999 [inlined]\n",
      " [10] _minimum\n",
      "    @ ./reducedim.jl:999 [inlined]\n",
      " [11] #_minimum#821\n",
      "    @ ./reducedim.jl:998 [inlined]\n",
      " [12] _minimum\n",
      "    @ ./reducedim.jl:998 [inlined]\n",
      " [13] #minimum#819\n",
      "    @ ./reducedim.jl:994 [inlined]\n",
      " [14] minimum(a::Vector{Real})\n",
      "    @ Base ./reducedim.jl:994\n",
      " [15] conv_plot1d(timed::Vector{Any}, α::Float64, offset::Int64)\n",
      "    @ Main ~/Documents/Arbeit/more-tests-adaptive-stoch-gal-fem/1DhigherorderPolynomials/PresentationTools.jl:135\n",
      " [16] conv_plot1d(timed::Vector{Any}, α::Float64)\n",
      "    @ Main ~/Documents/Arbeit/more-tests-adaptive-stoch-gal-fem/1DhigherorderPolynomials/PresentationTools.jl:129\n",
      " [17] top-level scope\n",
      "    @ ~/Documents/Arbeit/more-tests-adaptive-stoch-gal-fem/1DhigherorderPolynomials/Demo.ipynb:2"
     ]
    }
   ],
   "source": [
    "#convergence plot\n",
    "conv_plot1d(timed, α)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `PlotFE1D` plots a function given by its partition and nodal coefficients. In our case, these are the entries of `S[i]` and `u[i]`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "ename": "MethodError",
     "evalue": "MethodError: no method matching layout_args(::Int64, ::Tuple{Int64, Float64})\n\nClosest candidates are:\n  layout_args(::Integer)\n   @ Plots ~/.julia/packages/Plots/rz1WP/src/layouts.jl:437\n  layout_args(::Integer, !Matched::Integer)\n   @ Plots ~/.julia/packages/Plots/rz1WP/src/layouts.jl:447\n  layout_args(::Integer, !Matched::Union{Plots.GridLayout, AbstractVecOrMat})\n   @ Plots ~/.julia/packages/Plots/rz1WP/src/layouts.jl:484\n  ...\n",
     "output_type": "error",
     "traceback": [
      "MethodError: no method matching layout_args(::Int64, ::Tuple{Int64, Float64})\n",
      "\n",
      "Closest candidates are:\n",
      "  layout_args(::Integer)\n",
      "   @ Plots ~/.julia/packages/Plots/rz1WP/src/layouts.jl:437\n",
      "  layout_args(::Integer, !Matched::Integer)\n",
      "   @ Plots ~/.julia/packages/Plots/rz1WP/src/layouts.jl:447\n",
      "  layout_args(::Integer, !Matched::Union{Plots.GridLayout, AbstractVecOrMat})\n",
      "   @ Plots ~/.julia/packages/Plots/rz1WP/src/layouts.jl:484\n",
      "  ...\n",
      "\n",
      "\n",
      "Stacktrace:\n",
      " [1] layout_args(plotattributes::Dict{Symbol, Any}, n_override::Int64)\n",
      "   @ Plots ~/.julia/packages/Plots/rz1WP/src/layouts.jl:428\n",
      " [2] plot!(::Plots.Plot, ::Union{Plots.PlaceHolder, Plots.Plot}; kw::Base.Pairs{Symbol, V, Tuple{Vararg{Symbol, N}}, NamedTuple{names, T}} where {V, N, names, T<:Tuple{Vararg{Any, N}}})\n",
      "   @ Plots ~/.julia/packages/Plots/rz1WP/src/plot.jl:130\n",
      " [3] plot!\n",
      "   @ ~/.julia/packages/Plots/rz1WP/src/plot.jl:113 [inlined]\n",
      " [4] plot(::Plots.Plot{Plots.GRBackend}, ::Plots.Plot{Plots.GRBackend}; kw::Base.Pairs{Symbol, Tuple{Int64, Real}, Tuple{Symbol, Symbol}, NamedTuple{(:layout, :size), Tuple{Tuple{Int64, Float64}, Tuple{Int64, Int64}}}})\n",
      "   @ Plots ~/.julia/packages/Plots/rz1WP/src/plot.jl:112\n",
      " [5] top-level scope\n",
      "   @ ~/Documents/Arbeit/more-tests-adaptive-stoch-gal-fem/1DhigherorderPolynomials/Demo.ipynb:9"
     ]
    }
   ],
   "source": [
    "#function coefficients of different polynomials\n",
    "n = min(length(S),9)\n",
    "p = Array{Plots.Plot{Plots.GRBackend}, 1}(undef, n)\n",
    "\n",
    "for i in 1:n\n",
    "    PlotFE1D(S[i], u[i])\n",
    "    p[i] = plot!(title = decode(F[i]) , titlefontsize = 7)\n",
    "end\n",
    "plot(p... , layout = (3, Int(floor((n/3)))), size = (700, 700) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can evaluate our solution at any given point $y$. The file `PointwiseSolver.jl` contains functions for solving the model problem\n",
    "for a given $y$ and a given grid. We can thus compare the approximate solution `u` at any point to a pointwise solution on a finer grid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"PointwiseSolver.jl\")\n",
    "ℓ = 11\n",
    "N = 2^ℓ -1\n",
    "M = 6\n",
    "y = [2*rand(N).-1  for i in 1:M]\n",
    "plotDiffusionField(α, y)\n",
    "display(plot!(title = \"Diffusion coefficients\"))\n",
    "\n",
    "u_in_y, mesh = stocheval(F, S, u, y[1])\n",
    "u_in_y = [first(stocheval(F, S, u, y[i])) for i in 1:M]\n",
    "finemesh = children(mesh, 1)\n",
    "u_y_gal = PointwiseSolver1D(α , finemesh, y, zeros([finemesh for i in 1:M]),1e-8)\n",
    "\n",
    "diff_to_mean= first.([sum_u([1.0,-1.0],[u_in_y[i],u[1]],[mesh, S[1]]) for i in 1:M])\n",
    "diff_to_mean2 = first.([sum_u([1.0,-1.0],[u_y_gal[i],u[1]],[finemesh, S[1]]) for i in 1:M])\n",
    "diff_sol= first.([sum_u([1.0,-1.0],[u_y_gal[i],u_in_y[i]],[finemesh, S[1]]) for i in 1:M])\n",
    "\n",
    "PlotFE1D(mesh, diff_to_mean, false)\n",
    "display(plot!(title = \"Direct computation - diff to mean\"))\n",
    "PlotFE1D(finemesh, diff_to_mean2, false)\n",
    "display(plot!(title = \"Evaluation of approximation - diff to mean\"))\n",
    "PlotFE1D(finemesh, diff_sol, false)\n",
    "display(plot!(title = \"Difference of solutions\"))\n",
    "println(\"H1-norm of difference = \", normH1.([finemesh for i in 1:M], [meshDict(finemesh) for i in 1:M], diff_sol))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use this functionality to estimate the $L_2(Y)\\times H_0^1$-error by sampling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#MonteCarloScript\n",
    "start = 1\n",
    "timedMC = timed[1:start-1]\n",
    "for i in start:length(timed)\n",
    "    F = timed[i][5]\n",
    "    S = timed[i][6]\n",
    "    u = timed[i][7]\n",
    "    est, errors = MonteCarloEstimator(F, S, u, α, samples = 10)\n",
    "    push!(timedMC, (est, timed[i][2], timed[i][3], timed[i][4], timed[i][5], timed[i][6], timed[i][7] ))\n",
    "end\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The observed rates using MonteCarlo sampling closely match the ones produced by the error estimation using finite element frames:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "conv_plot1d(timedMC, α)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.9.1",
   "language": "julia",
   "name": "julia-1.9"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.9.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
