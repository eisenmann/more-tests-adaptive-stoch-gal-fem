# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


const LocalFE1D = Int64 #local hat function, 0 -> _\_ (right half, ≡1 in left node) , 1 -> _/_ (left half, ≡1 in right node)
const LocalFEs1D = 0:1
const LocalFEs = LocalFEs1D 
const GlobalFE1D = Tuple{Int64,Int64} #min Level of refiment where FE is piecewise poly, idx on uniform mesh



function localRep(fe::GlobalFE1D) #returns the support tiles for fe, respecting the tree order (like submesh)
	return ( ( (fe[1],fe[2]-1),1),( (fe[1],fe[2]),0) )
end
function getFEs(sm::SubMesh)
	ret = Vector{GlobalFE1D}()
	j = sm[1][1]
	@assert reduce(&, tile[1]==j for tile in sm )
	for i in 2:length(sm)
		if sm[i] == geometric_next(sm[i-1]) 
			push!(ret,(j,sm[i][2]) )
		end
	end
	return ret
end
function StaticFiniteElementsRoots()
	return [(1,1)]
end

function StaticFiniteElementsPseudoRoots()
	return [(0,1)]
end



function childrenframe(fe::GlobalFE1D)
	if fe[2] == 1
		return ( ( fe[1]+1, 2*fe[2]+i) for i in -1:1)
	else
		return ( ( fe[1]+1, 2*fe[2]+i) for i in 0:1)
	end
end

function ancestorframe(fe::GlobalFE1D)
	if fe[2] == 1
		return (fe[1] - 1, 1)
	else
		return (fe[1] - 1, fe[2] ÷ 2)
	end
end

function getSupport(fe::GlobalFE1D)
	((fe[1], fe[2]-1), (fe[1], fe[2]))
end
function children_geom(fe::GlobalFE1D)
	j,k = fe 
	return ((j+1,2k-1),(j+1,2k),(j+1,2k+1))
	getFEs(children([tile for (tile,locfe) in localRep(fe)]))
end
function refineRep(fe::GlobalFE1D) 
	children = children_geom(fe)
	return ((0.5,children[1]),(1.0,children[2]),(0.5,children[3])) #implementation depends on how getFEs orders the finite elements on a SubMesh
end

function localDGDim()
	return 2
end
function quadPoints()
	return 3
end


function val(tile::DyadicIndex1D, fe::LocalFE1D, x::Float64)
	a,b = getDomain(tile)
	if fe==0
		return (x-b)/(a-b)
	elseif fe==1
		return (x-a)/(b-a)
	end
end
function val∂(tile::DyadicIndex1D, fe::LocalFE1D, x::Float64)
	a,b = getDomain(tile)
	if fe==0
		return 1.0/(a-b)
	elseif fe==1
		return 1.0/(b-a)
	end
end

supportTiles(fe::GlobalFE1D) = ((fe[1],fe[2]-1),fe)


function val(tile::DyadicIndex1D, boundaryVals::Tuple{Float64,Float64}, x::Float64)
	a,b = getDomain(tile)
	return (x-b)/(a-b)*boundaryVals[1] + (x-a)/(a-b)*boundaryVals[2]
end


function translateTileValues(tile::DyadicIndex1D, boundaryVals::Tuple{Float64,Float64}, tile2::DyadicIndex1D)
	x1 = (tile2[2] - 2^(tile2[1] - tile[1])*tile[2]) * 0.5^(tile2[1]-tile[1])
	x2 = (tile2[2]+1 - 2^(tile2[1] - tile[1])*tile[2]) * 0.5^(tile2[1]-tile[1])
	return ( boundaryVals[1]*(1-x1) + boundaryVals[2]*x1 , boundaryVals[1]*(1-x2) + boundaryVals[2]*x2  )
end

function translateTileValues(tile::DyadicIndex1D, vals::SVector{polydegree+1,Float64}, tile2::DyadicIndex1D)
	x1 = (tile2[2] - 2^(tile2[1] - tile[1])*tile[2]) * 0.5^(tile2[1]-tile[1])
	x = lobattonodes*(0.5^(tile2[1]-tile[1])) .+x1
	return SVector{polydegree+1}(eval(xi,vals) for xi in x)
end

function val∂(tile::DyadicIndex1D, fe::LocalFE1D)
	if fe==0
		return -2.0^tile[1]
	elseif fe==1
		return 2.0^tile[1]
	end
end

function normH1(fe::GlobalFE1D)
	sqrt(2^fe[1])
end
