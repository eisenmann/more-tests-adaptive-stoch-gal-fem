# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis

#=
Relies on the funktions and types from tree.jl
Also relies on the functions:
- val 
- val∂
- localDGDim
- localDGfe
- localFEs
- idx
- localRep
- quadPoints
=#
using SparseArrays

include("PolynomialCalc.jl")


function meshDict(mesh::Mesh) #deterministic indexing of dofs on an (ordered) mesh, based on the first tile and the first node in this tile that sees this dof 
    idx = 1
    meshdict = fill(0,polydegree+1,length(mesh))
    dict = Dict{VertexIndexType, Int64}()
    for (j,tile) in enumerate(mesh)
        for (i,node) in zip((1,polydegree+1),simplifyVertex.(vertexes(tile)))
            if !onBoundary(node)
                if !haskey(dict, node)
                    # we have a new degree of freedom
                    dict[node] = idx
                    meshdict[i,j] = idx
                    idx += 1
                else
                    #we already met the degree of freedom in this node and gave it index dict[node]
                    meshdict[i,j] = dict[node]
                end
            end
        end
        for i in 2:polydegree
            meshdict[i,j] =idx
            idx +=1
        end
    end
    return meshdict
end

function localPoly(u::Vector{Float64}, meshdict:: Matrix{Int64}, i::Int64)
    indices = meshdict[:,i]
    if indices[1]==0 && indices[end] == 0
        return SVector{polydegree+1}([0.0; u[indices[2:end-1]];0.0])
    elseif indices[1]==0
        return SVector{polydegree+1}([0.0; u[indices[2:end]]])
    elseif indices[end]==0
        return SVector{polydegree+1}([u[indices[1:end-1]];0.0])
    else
        return SVector{polydegree+1}(u[indices])
    end
end

function idx(meshdict::MeshDict, fe::LocalFE1D, i::Int64) #idx starting with 1
	return meshdict[fe+1,i]
end
function localFEs(meshdict::Matrix{Int64}, i::Int64)
	LocalFEs[meshdict[:,i].>0] #pick out the FEs that are active
end
function dimFE(meshdict::MeshDict)
	return maximum(meshdict)
end

function integral(tile::DyadicIndex1D, u::SVector{polydegree+1, Float64}, v::SVector{polydegree+1, Float64})
    return 0.5^tile[1]*sum(u.*v.*lobattoweights)
end

function integralL2H1_1D(tile::DyadicIndex1D, ∂u::NTuple{2,Float64}, vloc::Tuple{DyadicIndex1D, Int64})
    if intersect(tile, vloc[1])
        sign = -1+2*vloc[2]
        if vloc[1][1]> tile[1]
            weight = (vloc[1][2]*2+1 - tile[2]*2^(1+vloc[1][1]-tile[1])) *0.5^(1+vloc[1][1]-tile[1][1])
            return sign * (weight* ∂u[2] + (1-weight)  *∂u[1])
        else    
            return sign * (∂u[1] + ∂u[2]) * 0.5^(-vloc[1][1]+tile[1]+1) 
        end
    end
    return 0.0
end

function integralL2H1_1D__fastforbpx(tile::DyadicIndex1D, ∂u::SVector{polydegree+1, Float64}, vloc::Tuple{DyadicIndex1D, Int64})
    sign = -1+2*vloc[2]
    vals = translateTileValues(tile, ∂u, vloc[1])
    return sign * sum(vals.*lobattoweights)
end

#vloc is always finer than tile
function integralL2H1_1D__fastforbpx(tile::DyadicIndex1D, ∂u::NTuple{2,Float64}, vloc::Tuple{DyadicIndex1D, Int64})
    sign = -1+2*vloc[2]

    weight = (vloc[1][2]*2+1 - tile[2]*2^(1+vloc[1][1]-tile[1])) *0.5^(1+vloc[1][1]-tile[1][1])
    return sign * (weight* ∂u[2] + (1-weight)  *∂u[1]) 
end

function next(mesh::Vector{DyadicIndexType}, i::Int64, bound::Int64)
    return i==bound ? 0 : i+1
end

function next(mesh::Vector{DyadicIndexType}, i::Int64)
    return next(mesh, i, length(mesh))
end

function next(meshi::Vector{DyadicIndexType}, meshj::Vector{DyadicIndexType}, i::Int64 , j::Int64, boundi::Int64, boundj::Int64)
    @assert intersect(meshi[i],meshj[j])
    if j<boundj && intersect(meshi[i],meshj[j+1])
        return i,j+1
    elseif i<boundi && intersect(meshi[i+1],meshj[j])
        return i+1,j
    else
        return next(meshi, i, boundi),next(meshj, j, boundj)
    end
end

function next(meshi::Vector{DyadicIndexType}, meshj::Vector{DyadicIndexType}, i::Int64 , j::Int64)
    return next(meshi, meshj, i, j, length(meshi), length(meshj))
end

function generateVector(mesh::Vector{DyadicIndexType}, f::Function)
    meshdict = meshDict(mesh)
    I = Int64[]; J = Int64[]; V = Float64[];
    i = 1
    while i >0 
        for lfe_i in localFEs(meshdict,i)
            push!(I, idx(meshdict,lfe_i,i) )
            push!(J, 1 )
            push!(V, f( (mesh[i], lfe_i) ))
        end
        i = next(mesh,i)
    end
    return vec(sparse(I, J, V))
end

function normH1(mesh::Mesh, meshdict::Matrix{Int64},u::Vector{Float64})
    s =0.0
    for i in 1:length(mesh)
        @views indices = meshdict[:, i]
        vals = lobattoDiffMatrix * SVector{polydegree+1}(idx == 0 ? 0.0 : u[idx] for idx in indices )
        s+= vals'*(vals.*lobattoweights) * 2^mesh[i][1]
    end
    return sqrt(s)
end





