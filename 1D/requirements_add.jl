using Pkg
Pkg.add("TimerOutputs")
Pkg.add("StaticArrays")
Pkg.add("DataStructures")
Pkg.add("StatProfilerHTML")
Pkg.add("Plots") #optional
Pkg.add("LegendrePolynomials") #optional
Pkg.add("LaTeXStrings") #optional