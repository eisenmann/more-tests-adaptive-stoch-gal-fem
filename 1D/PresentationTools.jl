# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


using Plots
using LegendrePolynomials
using LaTeXStrings
include("AdaptiveSolver.jl")

function PlotFE1D(mesh::Mesh, u::Vector{Float64}, scatter = true)
    x = [0.0]
    y = [0.0]
    append!(y, u)
    push!(y, 0.0)
    for tile in mesh
        push!(x, 0.5^tile[1]*(tile[2]+1))
    end
    plot(x,y)
    if scatter
        scatter!(x,y, color =:black, markersize = 1)
    end
    return plot!(legend = false, framestyle = :box, xlims = (0,1),   xtickfont=font(5), 
    ytickfont=font(5) )
end

function PlotFE1D(mesh::Mesh, U::Vector{Vector{Float64}}, scatter = true,   xtickfont=font(5), 
    ytickfont=font(5) )
    plot()
    for u in U
        x = [0.0]
        y = [0.0]
        append!(y, u)
        push!(y, 0.0)
        for tile in mesh
            push!(x, 0.5^tile[1]*(tile[2]+1))
        end
        plot!(x,y )
        if scatter
            scatter!(x,y, color =:black, markersize = 1)
        end
    end
    return plot!(legend = false, framestyle = :box, xlims = (0,1),   xtickfont=font(5), 
    ytickfont=font(5))
end

function sum_u(weights::Vector{Float64}, u::Vector{Vector{Float64}}, S::Vector{Mesh})
    meshdicts = meshDict.(S)
    allTiles = vcat(S...)
    finemesh = getMesh(makeMinTree(allTiles))
    finemeshdict = meshDict(finemesh)
    ur = sum(inclusion(S[i], finemesh, meshdicts[i], finemeshdict, weights[i]*u[i]) for i in eachindex(S))
    return ur, finemesh
end

function max_dim(F::MultiIndexMap)
    return max(1,(dim(F[i]) for i in 1:length(F))...)
end

function legendrePoly(ν::MultiIndex)
    y -> prod( Pl(y[μ],getValue(ν,μ))*sqrt(getValue(ν,μ)*2+1) for μ in 1:length(y) )
end

function stocheval(F,S,u,y)
    u_in_y, mesh = sum_u([legendrePoly(F[i])(y) for i in 1:length(F)], u, S)
    return u_in_y, mesh
end


function plotDiffusionField(α::Float64, Y::Vector{Vector{Float64}} )
    plot()
    for y in Y
        level  =  Int(log2(length(y)+1)) - 1
        X = 0: 0.5^(level+1) : 1
        θ = ones(length(X))
        c = 0.1
        Capprox = 0.02
        c_shifted = c*0.5^(0*α)
        opinfo = OperatorInfo(c_shifted, Capprox, α, IterHat1D)
        

        for l in 0:level
            scale = scaleFactor(opinfo, l)
            xindex = 1
            k = 1
            for  (n,x) in enumerate(X)
                if x*2^l > k  
                    k += 1
                end
                # println("f($x) = ", 1 - abs(2*2^l*x - 2*k + 1))
                # println("l = $l")
                # println("k = $k")
                θ[n] += y[get1DlevelRange(l)[k]] * scale * (1 - abs(2*2^l*x - 2*k + 1))
            end
        end
        plot!(X,θ, xlims = (0,1), ylims = (0.8,1.2))
    end
    return plot!()
end

function plotDiffusionField(α::Float64, y::Vector{Float64} )
    level  =  Int(log2(length(y)+1)) - 1
    X = 0: 0.5^(level+1) : 1
    θ = ones(length(X))
    c = 0.1
    Capprox = 0.02
	c_shifted = c*0.5^(0*α)
	opinfo = OperatorInfo(c_shifted, Capprox, α, IterHat1D)
    

    for l in 0:level
        scale = scaleFactor(opinfo, l)
        xindex = 1
        k = 1
        for  (n,x) in enumerate(X)
            if x*2^l > k  
                k += 1
            end
            # println("f($x) = ", 1 - abs(2*2^l*x - 2*k + 1))
            # println("l = $l")
            # println("k = $k")
            θ[n] += y[get1DlevelRange(l)[k]] * scale * (1 - abs(2*2^l*x - 2*k + 1))
        end
    end
    return plot(X,θ, xlims = (0,1), ylims = (0.8,1.2))
end

function conv_plot1d(timed, α, offset = 15)
    data = transpose(reshape( [timed[i][j] for i in eachindex(timed) for j in 1:4], 4, length(timed) ))
    plot(framestyle =:box)
    plot!(max.(data[offset:end,2],(1)), data[offset:end,1], label = "time", xscale=:log10, yscale=:log10, linestyle =:dashdot)
    plot!(data[offset:end,3], data[offset:end,1], label = L"\# F")
    plot!(data[offset:end,4], data[offset:end,1], label = L"\mathcal{N}(\mathbb{T})")
    
    minerr = minimum(data[offset:end,1])
    maxerr = maximum(data[offset:end,1])
    maxax = maximum(data[offset:end,4])
    maxax2 = maximum(data[offset:end,3])
    rate = α
    rate2 = 2/3*rate
    f(x) = x^(-rate) *  minerr*maxax2^rate 
    plot!(f, label=latexstring(L"rate $s=",round.(rate, digits = 3) , L"$"), xscale=:log10, yscale=:log10,linestyle=:dash)
    g(x) = x^(-rate2) *  minerr*maxax^rate2
    plot!(g, label=latexstring(L"rate $s=",round.(rate2, digits = 3) , L"$"), xscale=:log10, yscale=:log10,linestyle=:dash)
    plot!(ylims = (minerr, maxerr), xlims =(1, maxax), xticks = 10.0.^(0:floor(log10(maxax))))
    return plot!(title = latexstring("d = 1, \\alpha = $(round(α, digits = 3))"))
end

function MonteCarloEstimator(F::MultiIndexMap, S::Vector{Mesh}, u::FunctionCoeff, α::Float64; samples::Int64 = 100, extra_refinements::Int64 = 1, tolerance::Float64 = 1e-10, level::Int64 =  15)
    errors = Float64[]
    for it in 1:samples
        y = 2*rand(2^level-1) .- 1
        u_in_y, mesh = stocheval(F,S,u,y)
        finemesh = children(mesh, extra_refinements)
        u_y_ref = PointwiseSolver1D(α, finemesh, [y], zeros([finemesh]), tolerance)[1]
        diff_sol, mesh_d = sum_u([1.0,-1.0],[u_y_ref,u_in_y],[finemesh, mesh])
        push!(errors,normH1(mesh_d, meshDict(mesh_d), diff_sol))
        currEst = (norm(errors)/sqrt(it))
        currVar = 1
        
        if it >1
            currVar = norm(errors.^2 .- currEst^2)/sqrt(it-1)
        end
        println("Estimated error after $it samples is $currEst,  Control number is $currVar")
    end
    
    return norm(errors)/sqrt(samples), errors
end