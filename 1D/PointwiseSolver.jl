# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


function PointwiseSolver1D(α::Float64, mesh::Mesh, ys::Vector{Vector{Float64}}, u::FunctionCoeff, ϵ::Float64)
    c = 0.1

	Capprox = 0.02
	c_shifted = c*0.5^(0*α)
	opinfo = OperatorInfo(c_shifted, Capprox, α, IterHat1D)
    
    @printf("   [ entered solveGalerkinSystem")

    cnxData = prepareCNX(mesh)

    precond(w::FunctionCoeff) = [cnxPrecondition1D(ww, cnxData) for ww in w]
    meshdict = meshDict(mesh)
    Op(x, c, w) = [applySpatial1DgalPointwise!(mesh, meshdict, y , xx, opinfo, ww, c) for (xx,ww,y) in zip(x,w,ys)]

    f = [genconstrhs( mesh, meshdict) for y in ys]
    i, normr = cgiter!(Op, precond, f, u, ϵ)
    return u
end



# for MonteCarlo
# coeffs needs to be of length 2^j - 1 for some j
function applySpatial1DPointwise(mesh::Mesh, meshdict::Matrix{Int64}, y::Vector{Float64} , u::Vector{Float64}, opinfo::OperatorInfo)
    PP = piecewisePoly()
    level  =  Int(log2(length(y)+1)) - 1
    for l in -1:level
        scale = scaleFactor(opinfo, l)
        indexmap = zeros(Int64, 2 , length(get1DlevelRange(l)))
        if l ≥ 0 
            beforethislevel = 2^(l)-1
            for μ in get1DlevelRange(l)
                indexmap[1, μ - beforethislevel] = μ - beforethislevel
            end
        else
            indexmap[1,1] = 1
        end
        applyHatLevel!(l, u , [PP for i in get1DlevelRange(l)] , mesh, meshdict, indexmap, l == -1 ? [1.0] : scale* y[get1DlevelRange(l)])
    end
    DG = piecewisePolyToDiscontinuousGalerkin(PP) 
    
    return DG
end



# for CG method
function applySpatial1DgalPointwise!(mesh::Mesh, meshdict::Matrix{Int64}, y::Vector{Float64} , u::Vector{Float64}, opinfo::OperatorInfo, w::Vector{Float64}, c::Float64)
    PP = piecewisePoly()
    level  =  Int(log2(length(y)+1)) - 1
    for l in -1:level
        scale = scaleFactor(opinfo, l)

        indexmap = zeros(Int64, 2 , length(get1DlevelRange(l)))
        if l ≥ 0 
            beforethislevel = 2^(l)-1
            for μ in get1DlevelRange(l)
                indexmap[1, μ - beforethislevel] = μ - beforethislevel
            end
        else
            indexmap[1,1] = 1
        end
        magicfactor = 1
        applyHatLevel!(l, u , [PP for i in get1DlevelRange(l)] , mesh, meshdict, indexmap, l == -1 ? [1.0] : magicfactor*scale*y[get1DlevelRange(l)])
    end
    DG = piecewisePolyToDiscontinuousGalerkin(PP)
    w .+= c* discontinuousGalerkinToVector(mesh, meshdict, DG)

    return w
end



