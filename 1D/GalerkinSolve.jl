# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis

# routines for solving Galerkin discretized problems
# on fixed index sets
include("MultiIndex.jl")
include("MultiIndexMap.jl")
include("StochOperatorFEM.jl")
include("StochResidual.jl")



function cgiter!(A::Function, M::Function, f::FunctionCoeff, u::FunctionCoeff, ϵ::Real, maxiter::Int64 = 500)
    #set size
    z = deepcopy(f)
    # set initial r
    r = deepcopy(f)
    A( u, -1., r)
    z = M(r)
    p = deepcopy(z)
    r0 = dot(z, r)
    println("cgiter started with r: ", sqrt(r0))
    Ap = zeros(p)
    i = 0

    while r0 > ϵ^2 && i < maxiter
        setzero(Ap)
        A(p, 1., Ap)
        α = r0 / dot(Ap, p) # 1 / in H^{-1} , H^1
        
        u .+= α .* p

        r .-= α*Ap
        z .= M(r)
        r1 = dot(z,r) # in H^1  , H^{-1}
        p .*= (r1 / r0)
        p .+= z
        r0 = r1
        i+=1
    end
    if r0 > ϵ^2
        println("Warning maxiter reached in cgiter, with residual ", sqrt(r0))
    end
    println("cgiter used ", i, " iterations")
    println("cgiter ended with r: ", sqrt(r0))
    return u, i, sqrt(r0)
end


function applyGalerkinMatrixfree1D!(F::MultiIndexMap, S::Vector{Mesh},
    meshdicts::Vector{Matrix{Int64}}, opinfo::OperatorInfo, level::Int64,
    v::FunctionCoeff, c::Real, w::FunctionCoeff)  #where T
    applySpatial1Dgal!(F, S, meshdicts, level , v, opinfo, w, c)
    return w
end


struct cnxStruct
    nodeList::Vector{DyadicIndex1D}
    indexNodes::Dict{DyadicIndex1D,Int64}
    tilesNodes::Dict{DyadicIndex1D,Array{DyadicIndex1D}}
    parents::Dict{DyadicIndex1D,Array{DyadicIndex1D}}
    smoothers::Vector{Matrix{Float64}}
    diagsmoother::Vector{Float64}
end

function solveGalerkinSystem1D!(opinfo::OperatorInfo, F::MultiIndexMap, S::Vector{Mesh}, 
                            meshdicts::Vector{Matrix{Int64}}, ϵ::Real, u::FunctionCoeff, f::FunctionCoeff, L::Int64)
    @printf("   [ entered solveGalerkinSystem: F %d, S %d \n", length(S), sum(length(s) for s in S))

    cnxData = cnxStruct[]
    for i in 1:length(S)
        cnxi = prepareCNX(S[i])
        push!(cnxData, cnxi)
    end
    precond(w::FunctionCoeff) = [cnxPrecondition1D(ww, cnx) for (ww, cnx) in zip(w, cnxData)]
    Op(x, c, y) = applyGalerkinMatrixfree1D!(F,  S, meshdicts, opinfo, L, x, c, y)
    i, normr = cgiter!(Op, precond, f, u, ϵ/2)
    return u
end





function prepareCNX(mesh::Mesh)

    nodeList, indexNodes, tilesNodes, parents, index = getNodeList(mesh)
    diagsmoother = [1 / sum([2^(j(tile)) for tile in tilesNodes[nodeList[index[i]]]]) for i in 1:length(nodeList)]
    smoothers = Matrix{Float64}[]
    for node in nodeList
        Vindices = [indexNodes[node]]
        V = [node]
        #check if parents lie on boundary
        for p in parents[node]
            if j(p) != 0
                push!(Vindices,indexNodes[p])
                push!(V,p)
            end
        end 
        #build Matrices
        A = zeros(length(V),length(V))
        tiles = reduce(union, [tilesNodes[n] for n in V])
        for tile in tiles
            for (node1,n1) in zip(V,1:length(V))
                g1 = 0
                if tile in tilesNodes[node1]
                    a,b = getDomain(tile)
                    g1 = (2*getDomain(node1)[1]-a-b)/(b-a)^2
                    for (node2,n2) in zip(V,1:length(V))
                        g2 = 0
                        if tile in tilesNodes[node2]
                            g2 = (2*getDomain(node2)[1]-a-b)/(b-a)^2
                            A[n1,n2] += g1*g2*(b-a) 
                        end
                    end
                end
            end
        end
        push!(smoothers,inv(A))
        if k(node)!=1 && k(node) != 2^j(node)-1 
            for p in parents[node]
                for (tile,n) in zip(tilesNodes[p],1:length(tilesNodes[p]))
                    if tile in tilesNodes[node]
                        tilesNodes[p][n] = ancestor(tile)
                    end
                end
            end
        end
    end
    return cnxStruct(nodeList, indexNodes, tilesNodes, parents, smoothers, diagsmoother)
end
  
function cnxPrecondition1D(ww::Vector{Float64}, c::cnxStruct)
    w = deepcopy(ww)
    Dw = Diagonal(c.diagsmoother)*ww
    increments = Vector{Float64}[]
    for (node,n) in zip(c.nodeList,1:length(c.nodeList))
        Vindices = [c.indexNodes[node]]
        V = [node]
        #check if parents lie on boundary
        for p in c.parents[node]
            if j(p) != 0  
                push!(Vindices,c.indexNodes[p])
                push!(V,p) 
            end
        end 
        push!(increments,c.smoothers[n]*(w[Vindices]))
        #adjust tiles of parents
        if k(node)!=1 && k(node) != 2^j(node)-1 
            for p in c.parents[node]
                for (tile,n) in zip(c.tilesNodes[p],1:length(c.tilesNodes[p]))
                    if tile in c.tilesNodes[node]
                        c.tilesNodes[p][n] = ancestor(tile)
                    end
                end
            end
        end
        #adjust values of parents
        w[Vindices[2:end]].+=0.5*w[Vindices[1]]
    end
    Bw = 0*w
    for node in reverse(c.nodeList)
        Vindices = [c.indexNodes[node]]
        #check if parents lie on boundary
        for p in c.parents[node]
            if j(p) != 0
                push!(Vindices,c.indexNodes[p])
            end
        end 
        Bw[c.indexNodes[node]] = sum(Bw[Vindices])/2
        Bw[Vindices] +=pop!(increments)
    end
    return Dw + Bw
  
end
  
  
  function getNodeList(mesh)
      #1D
      nodeList = Vector{DyadicIndex1D}()
      tilesNodes = Dict{DyadicIndex1D,Array{DyadicIndex1D}}()
      parents = Dict{DyadicIndex1D,Array{DyadicIndex1D}}()
      for n in 2:length(mesh)
          node = deepcopy(mesh[n])
          while k(node) % 2 == 0
              node = (j(node)-1, Int(k(node)/2))
          end
          push!(nodeList, node)
          tilesNodes[node] = [mesh[n-1],mesh[n]]
          parents[node] = [(j(node)-1, floor(k(node)/2)), (j(node)-1, ceil(k(node)/2))]
          for n in 1:2
              while k(parents[node][n]) % 2 == 0 && j(parents[node][n]) >0
                  parents[node][n] = (j(parents[node][n])-1, Int(k(parents[node][n])/2))
              end
          end
      end
      indices = sortperm(j.(nodeList),rev = true)
      nodeList = nodeList[indices]
      indexNodes = Dict{DyadicIndex1D,Int64}(zip(nodeList, indices))
      return nodeList, indexNodes, tilesNodes, parents, sortperm(indices)
  end
  
