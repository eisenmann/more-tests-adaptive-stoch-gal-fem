{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3>Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients</h3>\n",
    "Released under MIT License, see <a href=\"https://git.rwth-aachen.de/bachmayr/asgfemmlrrc/-/blob/22af3f3c5089f492f83dbfcc4517f07492620236/README.md\">README.md</a><br/>\n",
    "Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our aim is to approximate the solution of the model problem\n",
    "$$\n",
    "    -\\partial_x \\cdot(a(x,y) \\partial_x u(x,y)) = f(x) \\quad\\text{for $x\\in (0,1)$, }\\\\\n",
    "    u(0,y) = u(1,y) = 0,\n",
    "$$\n",
    "where \n",
    "$$\n",
    "    a(x,y) = \\theta_0(x)  +  \\sum_\\mu y_\\mu\\theta_\\mu(x)\n",
    "$$\n",
    "is a multilevel representation of a random field.\n",
    "\n",
    "We first include the necessary files for approximating the solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"AdaptiveSolver.jl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `driver1D` takes as input $\\alpha$ representing the decay in the diffusion coefficient and `N` as a stopping criterium for the size $\\mathcal N(\\mathbb T)$. It computes an approximation to the solution of the model problem with constant right-hand side.\n",
    "\n",
    "The output `F, S, u, timed` contains the required polynomial multiindices in `F`, the family of partitions in `S`, the coefficients of the solution in `u` and data concerning the error, degrees of freedom, and computational time, as well as intermediate solutions in `timed`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 20000\n",
    "α = 2/3\n",
    "F, S, u, timed  = driver1D(α,N);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now load files for visualization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"PresentationTools.jl\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A convergence plot can be shown with the function `conv_plot1d` with the additional information of `timed` and $\\alpha$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#convergence plot\n",
    "conv_plot1d(timed, α)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `PlotFE1D` plots a function given by its partition and nodal coefficients. In our case, these are the entries of `S[i]` and `u[i]`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#function coefficients of different polynomials\n",
    "n = min(length(S),9)\n",
    "p = Array{Plots.Plot{Plots.GRBackend}, 1}(undef, n)\n",
    "\n",
    "for i in 1:n\n",
    "    PlotFE1D(S[i], u[i])\n",
    "    p[i] = plot!(title = decode(F[i]) , titlefontsize = 7)\n",
    "end\n",
    "plot(p... , layout = (3, floor(Int(n/3))), size = (700, 700) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can evaluate our solution at any given point $y$. The file `PointwiseSolver.jl` contains functions for solving the model problem\n",
    "for a given $y$ and a given grid. We can thus compare the approximate solution `u` at any point to a pointwise solution on a finer grid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "include(\"PointwiseSolver.jl\")\n",
    "ℓ = 11\n",
    "N = 2^ℓ -1\n",
    "M = 6\n",
    "y = [2*rand(N).-1  for i in 1:M]\n",
    "plotDiffusionField(α, y)\n",
    "display(plot!(title = \"Diffusion coefficients\"))\n",
    "\n",
    "u_in_y, mesh = stocheval(F, S, u, y[1])\n",
    "u_in_y = [first(stocheval(F, S, u, y[i])) for i in 1:M]\n",
    "finemesh = children(mesh, 1)\n",
    "u_y_gal = PointwiseSolver1D(α , finemesh, y, zeros([finemesh for i in 1:M]),1e-8)\n",
    "\n",
    "diff_to_mean= first.([sum_u([1.0,-1.0],[u_in_y[i],u[1]],[mesh, S[1]]) for i in 1:M])\n",
    "diff_to_mean2 = first.([sum_u([1.0,-1.0],[u_y_gal[i],u[1]],[finemesh, S[1]]) for i in 1:M])\n",
    "diff_sol= first.([sum_u([1.0,-1.0],[u_y_gal[i],u_in_y[i]],[finemesh, S[1]]) for i in 1:M])\n",
    "\n",
    "PlotFE1D(mesh, diff_to_mean, false)\n",
    "display(plot!(title = \"Direct computation - diff to mean\"))\n",
    "PlotFE1D(finemesh, diff_to_mean2, false)\n",
    "display(plot!(title = \"Evaluation of approximation - diff to mean\"))\n",
    "PlotFE1D(finemesh, diff_sol, false)\n",
    "display(plot!(title = \"Difference of solutions\"))\n",
    "println(\"H1-norm of difference = \", normH1.([finemesh for i in 1:M], [meshDict(finemesh) for i in 1:M], diff_sol))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use this functionality to estimate the $L_2(Y)\\times H_0^1$-error by sampling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#MonteCarloScript\n",
    "start = 1\n",
    "timedMC = timed[1:start-1]\n",
    "for i in start:length(timed)\n",
    "    F = timed[i][5]\n",
    "    S = timed[i][6]\n",
    "    u = timed[i][7]\n",
    "    est, errors = MonteCarloEstimator(F, S, u, α, samples = 10)\n",
    "    push!(timedMC, (est, timed[i][2], timed[i][3], timed[i][4], timed[i][5], timed[i][6], timed[i][7] ))\n",
    "end\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The observed rates using MonteCarlo sampling closely match the ones produced by the error estimation using finite element frames:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "conv_plot1d(timedMC, α)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.9.1",
   "language": "julia",
   "name": "julia-1.9"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.9.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
