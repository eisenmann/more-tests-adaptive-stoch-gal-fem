# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


import
    Base: length, in, push!, delete!, deleteat!, empty!, iterate


#MultiIndex to Integer and back
mutable struct MultiIndexMap
    ν::Vector{MultiIndex}
    n::Dict{MultiIndex, Int64}

    # constructor
    function MultiIndexMap()
        ν = Vector()
        n = Dict{MultiIndex, Int64}()
        new(ν, n)
    end

    function MultiIndexMap(ν::Vector{MultiIndex})
        n = Dict{MultiIndex, Int64}(zip( ν,1:length(ν) ))
        new(ν, n)
    end
end # type MultiIndexMap


at(map::MultiIndexMap, index::MultiIndex) = map.n[index] 
at(map::MultiIndexMap, i::Int64) = map.ν[i]

length(map::MultiIndexMap) = length(map.ν)

Base.merge(a::MultiIndexMap,b::MultiIndexMap) = MultiIndexMap( Vector(a.ν ∪ b.ν) )

in(map::MultiIndexMap, index::MultiIndex) = haskey(map.n, index)

function push!(map::MultiIndexMap, index::MultiIndex)
    if !haskey(map.n, index)
        push!(map.ν, index)
        map.n[index] = length(map.ν)
        return length(map.ν)
    else
        return map.n[index]
    end
end

function delete!(map::MultiIndexMap, index::Vector{Int64})
    for i ∈ index
        delete!(map.n, map.ν[i])
    end
    deleteat!(map.ν, index)
    return remap!(map)
end

function empty!(map::MultiIndexMap)
    empty!(map.ν)
    empty!(map.n)
end

function remap!(map::MultiIndexMap)
    old = sort!(collect(values(map.n)))
    for i = 1:length(map)
        map.n[map.ν[i]] = i
    end
    return old
end

# standard element access notation
Base.getindex(map::MultiIndexMap, index::MultiIndex) = at(map, index)
Base.getindex(map::MultiIndexMap, i::Int64) = at(map, i)

# iteration over MultiIndexMap: equivalent to
# iteration over MultiIndexMap.ν
Base.iterate(map::MultiIndexMap) = iterate(map.ν)
Base.iterate(map::MultiIndexMap, state) = iterate(map.ν, state)
