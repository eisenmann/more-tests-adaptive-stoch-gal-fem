# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


const LocalFE2D = Int64 #local hat function, 0 -> (≡1 in 90 deg corner) , 1 (≡1 in first 45 deg corner, clockwise from 90 deg corner)  , 2 (≡1 in first 45 deg corner, clockwise from 90 deg corner)
#= 0 ___ 1
    |  /
    | /
   2|/     =#
const LocalFEs2D = 0:2
const LocalFEs = LocalFEs2D

const GlobalFE2D = Tuple{Int64,Int64,Int64} #Level, horizontal idx, vertical index on uniform mesh
const LocalFE = LocalFE2D

if LSHAPED == true
	println("L Shaped domain (Lin FEM)")

	function StaticFiniteElementsRoots()
		return [(2,1,1),(2,2,1),(2,3,1),(2,3,2),(2,3,3)]
	end

	function isRoot(fe::GlobalFE2D)
		return fe[1]==2
	end

else
	println("Square domain (Lin FEM)")

	#=
	begin square mesh
	=#
	function StaticFiniteElementsRoots()
		return [(1,1,1)]

	end

	function isRoot(fe::GlobalFE2D)
		return fe[1]==1
	end
	#=
	end square mesh
	=#
end


function children(fe::GlobalFE2D)
	END = 2^fe[1]-1
	if fe[2] == END && fe[3] == 1
		return ( ( fe[1]+1, 2*fe[2]+i, 2*fe[3]+j ) for i in -1:1 for j in -1:1 )
	elseif fe[2] == END
		return ( ( fe[1]+1, 2*fe[2]+i, 2*fe[3]+j ) for i in -1:1 for j in 0:1 )
	elseif fe[3] == 1
		return ( ( fe[1]+1, 2*fe[2]+i, 2*fe[3]+j ) for i in -1:0 for j in -1:1 )
	else
		return ( ( fe[1]+1, 2*fe[2]+i, 2*fe[3]+j ) for i in -1:0 for j in 0:1 )
	end
end
function ancestor(fe::GlobalFE2D)
	if fe[1] == 1
		return nothing
	else
		return (fe[1]-1, max(1, (fe[2]+1)÷2), max(1, fe[3]÷2))
	end
end



function localRep(fe::GlobalFE2D) #returns the support tiles for fe, respecting the tree order (like submesh)
	if iseven(fe) #treat LocalFE2D as VertexIndex2D
		return ( ( ( fe[1], fe[2]-1, fe[3]-1,0), 1), (( fe[1], fe[2]-1, fe[3]-1,1), 2),  #still needs to be sorted!
			 	 ( ( fe[1], fe[2], fe[3]-1,0), 2),   (( fe[1], fe[2], fe[3]-1,1),  1),
				 ( ( fe[1], fe[2]-1, fe[3],0), 1),   (( fe[1], fe[2]-1, fe[3],1),  2),
				 ( ( fe[1], fe[2], fe[3],  0), 2),   (( fe[1], fe[2], fe[3],1), 1)
		)
	else
		return (( ( fe[1], fe[2]-1, fe[3]-1,1), 0),  #still needs to be sorted!
				( ( fe[1], fe[2], fe[3]-1,1), 0), 
				( ( fe[1], fe[2]-1, fe[3],0), 0), 
				( ( fe[1], fe[2], fe[3],  0), 0) 
		)
	end
end
function supportTiles(fe::GlobalFE2D)
	if iseven(fe) #treat LocalFE2D as VertexIndex2D
		return (  ( fe[1], fe[2]-1, fe[3]-1,0), ( fe[1], fe[2]-1, fe[3]-1,1),  #still needs to be sorted!
			 	  ( fe[1], fe[2], fe[3]-1,0),   ( fe[1], fe[2], fe[3]-1,1),
				  ( fe[1], fe[2]-1, fe[3],0),   ( fe[1], fe[2]-1, fe[3],1),
				  ( fe[1], fe[2], fe[3],  0),   ( fe[1], fe[2], fe[3],1)
		)
	else
		return ( ( fe[1], fe[2]-1, fe[3]-1,1),  #still needs to be sorted!
				 ( fe[1], fe[2], fe[3]-1,1), 
				 ( fe[1], fe[2]-1, fe[3],0), 
				 ( fe[1], fe[2], fe[3],  0) 
		)
	end
end
function getFEs(sm::SubMesh)
	ret = Vector{GlobalFE2D}()
	j = sm[1][1]
	@assert reduce(&, tile[1]==j for tile in sm ) #tiles on same level
	@assert j % 2 ==0
	vertexToTiles=Dict{VertexIndex2D,TriangleIndex2D}()
	for tile in sm
		for vertex in vertexes(tile)
			if haskey(vertexToTiles,vertex)
				push!(vertexToTiles[vertex],tile)
			else
				vertexToTiles[vertex]= [tile]
			end
		end
	end

	for vertex in keys(vertexToTiles)
		if supportTiles(vertex) ⊂ vertexToTiles[vertex] #check that the support is contained
			push!(ret,vertex)
		end
	end
	return ret
end

function children_geom(fe::GlobalFE2D)
	return last.(refineRep(fe))
	if iseven(fe)
		return (
			(fe[1]+1,fe[2]*2,fe[3]*2),
			(fe[1]+1,fe[2]*2-1,fe[3]*2-1),
			(fe[1]+1,fe[2]*2,fe[3]*2-1),
			(fe[1]+1,fe[2]*2+1,fe[3]*2-1),
			(fe[1]+1,fe[2]*2+1,fe[3]*2),
			(fe[1]+1,fe[2]*2+1,fe[3]*2+1),
			(fe[1]+1,fe[2]*2,fe[3]*2+1),
			(fe[1]+1,fe[2]*2-1,fe[3]*2+1),
			(fe[1]+1,fe[2]*2-1,fe[3]*2)
			)
	else
		(
			(fe[1]+1,fe[2]*2,fe[3]*2),
			(fe[1]+1,fe[2]*2,fe[3]*2-1),
			(fe[1]+1,fe[2]*2+1,fe[3]*2),
			(fe[1]+1,fe[2]*2,fe[3]*2+1),
			(fe[1]+1,fe[2]*2-1,fe[3]*2)
			)
	end
end

function refineRep(fe::GlobalFE2D) 
	if iseven(fe)
		return (
			(1.0, (fe[1]+1,fe[2]*2,fe[3]*2)),
			(0.5, (fe[1]+1,fe[2]*2-1,fe[3]*2-1)),
			(0.5, (fe[1]+1,fe[2]*2,fe[3]*2-1)),
			(0.5, (fe[1]+1,fe[2]*2+1,fe[3]*2-1)),
			(0.5, (fe[1]+1,fe[2]*2+1,fe[3]*2)),
			(0.5, (fe[1]+1,fe[2]*2+1,fe[3]*2+1)),
			(0.5, (fe[1]+1,fe[2]*2,fe[3]*2+1)),
			(0.5, (fe[1]+1,fe[2]*2-1,fe[3]*2+1)),
			(0.5, (fe[1]+1,fe[2]*2-1,fe[3]*2))
			)
	else
		(
			(1.0, (fe[1]+1,fe[2]*2,fe[3]*2)),
			(0.5, (fe[1]+1,fe[2]*2,fe[3]*2-1)),
			(0.5, (fe[1]+1,fe[2]*2+1,fe[3]*2)),
			(0.5, (fe[1]+1,fe[2]*2,fe[3]*2+1)),
			(0.5, (fe[1]+1,fe[2]*2-1,fe[3]*2))
			)
	end
end
#this can in principle be precomputed: Compute for one level, and adjust scaling
function val∂(tile::TriangleIndex2D, localFE::Int64)
    #if we use coordinates of vertices, we need to solve a two times two system
    vertices = vertexes(tile)
    Δ1 = coords(vertices[2]).- coords(vertices[1])
    Δ2 = coords(vertices[3]).- coords(vertices[1])
    b1,b2 = (0.,0.)
    if localFE == 0
        b1,b2 = (-1.,-1.)
    elseif localFE == 1
        b1 = 1.
    elseif localFE == 2
        b2 = 1.
    end
    det = Δ1[1]*Δ2[2] - Δ2[1]*Δ1[2]

    return (b1 * Δ2[2]- b2 * Δ1[2], b2 * Δ1[1] - b1 * Δ2[1])./ det
end

function normH1(fe::GlobalFE2D)
	return 2.0
end


#Takes piecewise linear function on nodes, and returns vector of piecewise constant function on triangles
function grad(u::Vector{Float64}, mesh::Mesh, meshdict::Matrix{Int64})
    ux = zeros(length(mesh))
    uy = zeros(length(mesh))
    for (i,tile) in enumerate(mesh)
        for j in 1:3
            if meshdict[j,i] != 0 
                g = val∂(tile, j-1)
                ux[i],uy[i] = (ux[i],uy[i]) .+ u[meshdict[j,i]] .* g  
            end
        end
    end
    return ux, uy
end

function div(ux::Vector{Float64}, uy::Vector{Float64}, mesh::Mesh, meshdict::Matrix{Int64})
    v = zeros(maximum(meshdict))
    for (i,tile) in enumerate(mesh)
        for j in 1:3
            if meshdict[j,i] != 0 
                g = val∂(tile, j-1)
                
                v[meshdict[j,i]] += (g[1]*ux[i] + g[2]*uy[i])*area(tile)
            end
        end
    end 
    return v
end

#bilinear element
#= base	=	1 ___ 2
    		 |   |
    	 	 | 	 |
   		    4|___|3 =#

function evalBilinear(square::SquareIndex2D, values::NTuple{4, Real}, x::NTuple{2, Real})
	base = coords(square)
	h = 2.0^(-square[1])
	relx = (x[1]-base[1])/h
	rely = -(x[2]-base[2])/h
	#coeffs = (values[1], values[2]-values[1],values[4]-values[1], values[1] -values[2] + values[3]-values[4])
	#return sum(coeffs.*(1,relx, rely, relx*rely))
	return values[1]+(values[2]-values[1])*relx + (values[4]-values[1])*rely + (values[1] -values[2] + values[3]-values[4])*relx*rely
end

function evalBilinear(square::SquareIndex2D, values::NTuple{4, Real}, x::NTuple{3,NTuple{2, Real}})
	base = coords(square)
	h = 2.0^(-square[1])
	relxs = (first.(x).-base[1])./h
	relys = (last.(x).-base[2])./(-h)
	#coeffs = (values[1], values[2]-values[1],values[4]-values[1], values[1] -values[2] + values[3]-values[4])
	#return (sum(coeffs.*(1,relx, rely, relx*rely)) for (relx, rely) in zip(relxs,relys))
	return (values[1]+(values[2]-values[1])*relx + (values[4]-values[1])*rely + (values[1] -values[2] + values[3]-values[4])*relx*rely for (relx, rely) in zip(relxs,relys) )
end

#=			1 ___ 1'___2
    		 |   |	  |
    	 	 | 	 |	  |
   		   4'|___|5'__|2' 
			 |   |	  |
    	 	 | 	 |	  |
   		    4|___|3'__|3
=#

function refineBilinear(values::NTuple{4, Real})
	newvalues = ((values[1]+values[2])/2, (values[3]+values[2])/2, 
						(values[4]+values[3])/2, (values[1]+values[4])/2, sum(values)/4)
	return (values[1],newvalues[1], newvalues[5], newvalues[4]),
			(newvalues[1], values[2], newvalues[2],newvalues[5]),
			(newvalues[5],newvalues[2], values[3], newvalues[3]),
			(newvalues[4],newvalues[5], newvalues[3], values[4])
end

barycentricBilinear(values::NTuple{4,Real}, λ, μ) = 
		values[1]*(1-λ)*(1-μ) + values[2]*λ*(1-μ) + values[3]*λ*μ + values[4]*(1-λ)*μ
	

function translateBilinear(values::NTuple{4,Real}, oldSquare::SquareIndex2D, newSquare::SquareIndex2D)
	leveldiff = newSquare[1] - oldSquare[1]
	λ_1 = newSquare[2]*0.5^leveldiff - oldSquare[2]
	μ_1 = newSquare[3]*0.5^leveldiff - oldSquare[3]
	λ_2 = (newSquare[2]+1)*0.5^leveldiff - oldSquare[2]
	μ_2 = (newSquare[3]+1)*0.5^leveldiff - oldSquare[3]
	b1 = barycentricBilinear(values, λ_1,μ_1)
	b2 = barycentricBilinear(values, λ_2,μ_1)
	b3 = barycentricBilinear(values, λ_2,μ_2)
	b4 = barycentricBilinear(values, λ_1,μ_2)
	return (b1, b2 , b3 , b4)
end

function translateBilinear(values::NTuple{4,Real}, oldSquare::TriangleIndex2D, newSquare::TriangleIndex2D)
	leveldiff = newSquare[1] - oldSquare[1]
	λ_1 = newSquare[2]*0.5^leveldiff - oldSquare[2]
	μ_1 = newSquare[3]*0.5^leveldiff - oldSquare[3]
	λ_2 = (newSquare[2]+1)*0.5^leveldiff - oldSquare[2]
	μ_2 = (newSquare[3]+1)*0.5^leveldiff - oldSquare[3]
	b1 = barycentricBilinear(values, λ_1,μ_1)
	b2 = barycentricBilinear(values, λ_2,μ_1)
	b3 = barycentricBilinear(values, λ_2,μ_2)
	b4 = barycentricBilinear(values, λ_1,μ_2)

	return (b1, b2 , b3 , b4)
end