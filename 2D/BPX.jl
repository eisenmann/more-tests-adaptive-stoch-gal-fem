# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


function H1dualNorm(BPXcoeffs::Dict{GlobalFE2D, Float64})
	sqrt(sum(c^2 for (k,c) in BPXcoeffs))
end

function getBPXcoeffs2D(r::DGfunction2D, constrhs::Bool = false)
	#first collect the needed FEs 
	framehierarchy = getFEframehierarchy(r[1])
	meshIndices = Dict{DyadicIndexType, Int64}(zip(r[1],1:length(r[1])))

	c = Dict{GlobalFE2D, Float64}()
	sizehint!(c, sum(length.(framehierarchy)))
	for fes in reverse!(framehierarchy)
		for fe in fes
			c[fe] = BPXcoeffs!(fe, c, constrhs, r, meshIndices)
		end
	end
	# no rescaling necessary because of 2D
	return c
end


function BPXcoeffs!(fe::GlobalFE2D, c::Dict{GlobalFE2D, Float64}, constrhs::Bool, r::DGfunction2D, meshIndices::  Dict{DyadicIndexType, Int64})
	refRep  = refineRep(fe)
	isrefined  = true
	for (coeff,fe_ref) in refRep 
		isrefined = haskey(c, fe_ref)
		if !isrefined
			break
		end
	end
	if isrefined
		return sum(coeff*c[fe_ref] for (coeff,fe_ref) in refineRep(fe) )
	else
		#=
		find the support triangles of fe
		-fe is finer than grid, hence we only need to make triangles of fe coarser
			to eventually find them in meshIndices
		=#
		s = 0.0
		
		locRep = localRep(fe)
		for feloc in locRep
			tile = feloc[1]
			if constrhs
				#consthrhs is equal to 1
				s += area(tile)*0.3333333333333333333333333333
			end
			while !haskey(meshIndices,tile)
				tile = ancestor(tile)
			end
			index = meshIndices[tile]
			ux,uy = r[2][index]
			s -= integralL2H1_2D__fastforbpx(tile, ux, uy, feloc)
		end
		return s * 0.5
	end
end

function getFEframehierarchy(mesh::Mesh)
	level = 4
	finemesh = reduce(vcat, children.(mesh,level))
	meshtree = getMeshTree(finemesh)
	framehierarchy = Vector{Vector{GlobalFE2D}}()
	new_elements =  Set(StaticFiniteElementsRoots())
	while length(new_elements) > 0 
		push!(framehierarchy, collect(new_elements) )
		empty!(new_elements)
		for fe in framehierarchy[end]
			supp_fe = supportTiles(fe)
			to_be_refined = false
			for tile in supp_fe
				to_be_refined = tile in meshtree
				if to_be_refined 
					break
				end
			end
			if to_be_refined
				union!(new_elements,children_geom(fe))
			end
        end
	end
	return framehierarchy
end	

function getMeshTree(mesh::Mesh)
	roots = StaticRoots()
	tree = Set{TriangleIndex2D}(roots )
	for a in mesh
		if a[4]>1
			a = ancestor(a)
		end
		while !in(a,tree)
		    push!(tree,a)
			a = grandancestor(a)  
        end
    end
	return tree
end