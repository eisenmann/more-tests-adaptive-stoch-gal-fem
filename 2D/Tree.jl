# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


#=required:
DyadicIndexType
NodeIndexType
j(DyadicIndexType) - Level
k() - comparable index, using < within one level, order relation should be preserved for children
children(idx )
ancestor
getDomain
common_ancestor
geometric_next - for iterator
=#


#is always assumed to be disjoint and may be assumed to be a covering (of the entire unit line [0,1]) by some routines.

#tree with inner nodes as keys and (a vector of) children as values - e.g. empty tree is only the root
#keys are inner nodes, leaves are the only nodes that are only children (of some key) and not
const treeDict = Dict{DyadicIndexType, Vector{DyadicIndexType}}
const piecewisePoly = Dict{DyadicIndexType,Vector{Float64}}
const Tiling = Vector{DyadicIndexType}
const SubMesh = Vector{DyadicIndexType} #Vector of sorted (left to right in tree) tiles, that discretise only part of the getDomain
const MeshDict = Matrix{Int64} # a dictionary for indexes column is the tile and row is the local index
const SmartMesh = Tuple{Mesh, MeshDict, Int64, treeDict, Dict{DyadicIndexType, Int64}} #Mesh, Index for DoFs, Fe Dim, tree for the mesh, map leaves -> index in mesh

#starting from here, the routines should not depend on the spatial dimension and work depending on a proper implementation 
# of rourines like children, ancestor, 
function isRoot(idx::DyadicIndexType)
    return j(idx) == 0
end

function children_abs(idx::DyadicIndexType, l::Integer)
    @assert (j(idx)≤l)
    return children(idx,l-j(idx))
end

function children(tiles::Tiling, l::Integer=1)
    return reduce(vcat, [children(tile, l) for tile in tiles])
end

function ancestor_abs(idx::DyadicIndexType,l::Integer)
    @assert (j(idx)≥l)
    return ancestor(idx,j(idx)-l)
end

function intersect(tiles::Tiling, tile2::DyadicIndexType)
    ans = false
    for tile1 in tiles
        if intersect(tile1,tile2)
            ans = true
        end
    end
    return ans
end
function intersection(tile1::DyadicIndexType,tile2::DyadicIndexType)
    @assert intersect(tile1, tile2)
    return j(tile1) > j(tile2) ? tile1 : tile2
end

function strictlylessintree(tile1::DyadicIndexType, tile2::DyadicIndexType)
    anc1 = deepcopy(tile1)
    anc2 = deepcopy(tile2)
    if j(anc1) ≠ j(anc2)
        level = min(j(tile1),j(tile2))
        anc1 = ancestor_abs(tile1,level)
        anc2 = ancestor_abs(tile2,level)
    end
    simblings = j(anc1)==0 ? StaticRoots() : children(ancestor(anc1))
    while anc2 ∉ simblings
        anc1 = ancestor(anc1)
        anc2 = ancestor(anc2)
        simblings = j(anc1)==0 ? StaticRoots() : children(ancestor(anc1))
    end
    return indexin(anc1,simblings) < indexin(anc2,simblings)

end

function longEdgeEven(tile::TriangleIndex2D)
    return isDiag(longEdge(tile)) ? isDiagEven(tile) : iseven(tile)
end

function updateEdges2D!(edges::Dict{Tuple{EdgeIndex2D,Bool}}, tile::TriangleIndex2D)
    for e in (longEdge(tile),shortEdges(tile)...)
        vertHorEven = iseven(tile)
        diagEven = isDiagEven(tile)
        edges[(e, isDiag(e) ? diagEven : vertHorEven) ] = tile
    end
end
function deleteEdges2D!(edges::Dict{Tuple{EdgeIndex2D,Bool}}, tile::TriangleIndex2D)
    for e in (longEdge(tile),shortEdges(tile)...)
        vertHorEven = iseven(tile)
        diagEven = isDiagEven(tile)
        delete!(edges,(e, isDiag(e) ? diagEven : vertHorEven) )
    end
end

function removeHangingNodes(tiling::Tiling)
    levelwiseTiles = Vector{Set{DyadicIndexType}}()
    for tile in tiling
        lvl = level(tile)+1
        while length(levelwiseTiles) < lvl
            push!(levelwiseTiles,Set{DyadicIndexType}())
        end
        push!(levelwiseTiles[lvl],tile)
    end
    push!(levelwiseTiles,Set{DyadicIndexType}())
    edgeDict = Dict{Tuple{EdgeIndex2D,Bool},DyadicIndexType}()
    toRefine = Dict{EdgeIndex2D,DyadicIndexType}()
    for levelIdx in 1:length(levelwiseTiles)-2
        empty!(toRefine)
        for tile in levelwiseTiles[levelIdx]
            updateEdges2D!(edgeDict,tile)
            for edge in innerEdges(tile)
                if haskey(toRefine,edge)
                    delete!(toRefine,edge)
                else
                    toRefine[edge] = tile
                end
            end
        end
        if levelIdx-1 ≥ 1               
            for tile in levelwiseTiles[levelIdx-1]
                for edge in innerEdges(tile)
                    delete!(toRefine,edge)
                end
            end
        end
        for tile in levelwiseTiles[levelIdx+1]
            for edge in innerEdges(tile)
                delete!(toRefine,edge)
            end
        end
        for (edge, triangle) in toRefine
            done = false
            while !done
                innerLevelIdx = level(triangle) + 1
                cldrn = children(triangle)
                delete!(levelwiseTiles[innerLevelIdx],triangle)
                deleteEdges2D!(edgeDict, triangle) 
                if edge == longEdge(triangle)
                    push!(levelwiseTiles[innerLevelIdx+1],cldrn[1])
                    push!(levelwiseTiles[innerLevelIdx+1],cldrn[2])
                    updateEdges2D!(edgeDict,cldrn[1])
                    updateEdges2D!(edgeDict,cldrn[2])
                else
                    if longEdge(cldrn[1]) == edge 
                        push!(levelwiseTiles[innerLevelIdx+2],children(cldrn[1])[1])
                        updateEdges2D!(edgeDict,children(cldrn[1])[1])
                        push!(levelwiseTiles[innerLevelIdx+2],children(cldrn[1])[2])
                        updateEdges2D!(edgeDict,children(cldrn[1])[2])
                        push!(levelwiseTiles[innerLevelIdx+1],cldrn[2])
                        updateEdges2D!(edgeDict,cldrn[2])
                    elseif longEdge(cldrn[2]) == edge
                        push!(levelwiseTiles[innerLevelIdx+2],children(cldrn[2])[1])
                        updateEdges2D!(edgeDict,children(cldrn[2])[1])
                        push!(levelwiseTiles[innerLevelIdx+2],children(cldrn[2])[2])
                        updateEdges2D!(edgeDict,children(cldrn[2])[2])
                        push!(levelwiseTiles[innerLevelIdx+1],cldrn[1])
                        updateEdges2D!(edgeDict,cldrn[1])
                    else
                        @assert false
                    end
            
                end
                if haskey(edgeDict, (longEdge(triangle), !longEdgeEven(triangle)))
                    edge = longEdge(triangle)
                    triangle = edgeDict[(longEdge(triangle), !longEdgeEven(triangle))]
                else
                    done = true 
                end 
            end
        end
    end
    
    return  getMesh(makeMinTree(collect(reduce(union,levelwiseTiles))))
end

function CumulativeVals!(Vals::Dict{GlobalFE2D,Float64})
	#first sort tiles into level buckets
	levelFEs = Vector{Vector{GlobalFE2D}}()
	for fe in keys(Vals)
		while length(levelFEs) ≤ fe[1]
			push!(levelFEs,Vector{GlobalFE2D}())
		end
		push!(levelFEs[fe[1]], fe) 
	end
    levelFEs[1] = StaticFiniteElementsRoots()
	for l in length(levelFEs):-1:2
		for fe in levelFEs[l]
			a = ancestor(fe)
            if haskey(Vals,a)
                Vals[a] += Vals[fe]
            else
                Vals[a] = Vals[fe]
            end
		end	
	end
end

function getMeshFromBPX(Fext, C::Vector{Dict{GlobalFE2D,Float64}}, leaves::Vector{Tuple{Int64,GlobalFE2D}} , c_th::Float64)
    
    for i in 1:length(C)
        for (fe,val) in C[i]
            C[i][fe] = val^2
        end
    end
    for (i,fe) in leaves
        if !haskey(C[i],fe)
            C[i][fe] = 0.0
        end
    end
    CumulativeVals!.(C)
    leaves = thresholding( C, leaves, c_th)

    idxes = [isRoot(fe) ? 0 : i for (i,fe) in leaves]  # write Fext-index if there is some refinement for this index.
	unique!(idxes)
	sort!(idxes)
	if idxes[1]==0
		deleteat!(idxes,1)
	end
	#idxes now contains the sub-list of all Fext we want to keep

	S = Vector{Mesh}(undef, length(idxes))
	for i in 1:length(S)
		S[i] = Mesh()
	end

	idxinv = Dict([j => i for (i,j) in enumerate(idxes)])
	for (i,fe) in leaves
		if haskey(idxinv,i)
			append!(S[idxinv[i]],supportTiles(fe))
		end
	end

	for i in 1:length(S)
        S[i] = getMesh(makeMinTree(S[i]))
		S[i] = removeHangingNodes(S[i])
	end

	newleaves = Vector{Tuple{Int64,GlobalFE2D}}()
	for (i,fe) in leaves
		if haskey(idxinv,i)
			push!(newleaves,(idxinv[i],fe))
		end
	end

    return S, MultiIndexMap([Fext[i] for i in idxes]), newleaves


end


function refine(tiling::Tiling, mark::Vector{Bool},l::Integer=1)
    if l>0
        len = (length(mark) - count(mark))+count(mark)*2^(l)
        refined = Tiling(undef,len)
        i = 0
        for (flag,idx) in zip(mark,tiling)
            if flag
                children_i = children(idx,l)
                len_i = length(children_i)
                refined[i+1:i+len_i] = children_i
                i += len_i
            else
                i += 1
                refined[i] = idx
            end
        end
        return refined
    elseif l==0
        return deepcopy(tiling)
    else
        throw("negative refinement")
    end
end



function refine(tiling::Tiling,l::Integer=1)
    return refine(tiling,[true for t in tiling],l)
end

#makes a tree from leaves, only for disjoint tiles
function makeTree(tiles::Tiling)
    tree = treeDict()
    while length(tiles) > 0
        deleteat!(tiles, [isRoot(t) for t in tiles])
        for t in tiles
            a = ancestor(t)
            if haskey(tree,a)
                if t ∉ tree[a]
                    push!(tree[a],t)
                end
            else
                tree[a] = [t]
            end
        end
        tiles = ancestor.(tiles)
        unique!(tiles)
    end
    return tree
end


#makes minimal tree from set of tiles
function makeMinTree(tiles::Tiling)
    tree = treeDict()
    while length(tiles) > 0
        deleteat!(tiles, [isRoot(t) for t in tiles])
        for t in tiles
            a = ancestor(t)
            if haskey(tree,a)
                if t ∉ tree[a]
                    push!(tree[a],t)
                end
            else
                tree[a] = [t]
            end
        end
        tiles = ancestor.(tiles)
        unique!(tiles)
    end
    return tree
end

#makes minimal tree from set of tiles and sums the values on children

function removeLeaves(tree::treeDict)
    return makeMinTree(collect(keys(tree)))
end

function makeCumulativeTree(tiles::Tiling, vals::Dict{DyadicIndexType,Float64})
    tree = makeMinTree(tiles)
    return makeCumulativeTree(tree, vals)
end

function makeCumulativeTree(tree::Dict{DyadicIndexType,Vector{DyadicIndexType}}, vals::Dict{DyadicIndexType,Float64})
    #tree = makeMinTree(tiles)
    valtree = deepcopy(vals)
    while length(tree) > 0          #iterate through the tree from leaves to root
        leaves = getMesh(tree)
        for node in leaves
            a = ancestor(node)
            if haskey(valtree, a)
                valtree[a] += valtree[node]
            else
                valtree[a] = valtree[node]
            end
        end
        tree = removeLeaves(tree)
    end
    return valtree
end

#get leaves as SubMesh from subtree starting at a node
function getLeaves(tree::treeDict, nodes::Vector{DyadicIndexType})
    @assert reduce(&, [isRoot(node) || haskey(tree, ancestor(node)) for node in nodes] )
    stack = deepcopy(nodes)
    submesh = SubMesh()
    while length(stack) > 0 
        s = pop!(stack)
        if haskey(tree,s)
            append!(stack, reverse(childrenTuple(s)) )
        else
            push!(submesh,s)
        end
    end
    return submesh
end

function getLeaves(tree::treeDict, node::DyadicIndexType)
    getLeaves(tree, [node])
end

function findIntersection(tree::treeDict, node::DyadicIndexType)
    if isRoot(node) || haskey(tree, ancestor(node)) 
        return getLeaves(tree, node)
    else
        anc = deepcopy(node)
        while !(isRoot(anc) || haskey(tree, ancestor(anc)) )
            anc = ancestor(anc)
        end
        if haskey(tree, anc) && intersect(node,tree[anc])
            return [tree[anc]]
        else
            return [node][1:0]
        end
    end
end




#get mesh from tree
function getMesh(tree::treeDict)
    getLeaves(tree, reverse(StaticRoots()) )
end

function getMesh(tiles::Tiling)
    getMesh( makeTree(tiles) )
end


function height(tree::treeDict)
    return max(0, maximum([v[1][1] for (k,v) in tree]))+1
end
#get levelwise submeshes from tree
function getSubMeshes(tree::treeDict)

    stack = StaticRoots()
    submeshes = Vector{SubMesh}(undef, height(tree))
    for i in 1:length(submeshes) 
        submeshes[i] = SubMesh()
    end
    while length(stack) > 0 
        s = pop!(stack)
        if haskey(tree,s)
            append!(stack, reverse!(children(s)) )
        end
        push!(submeshes[ s[1]+1 ],s)
    end
    return submeshes
end

function makeTree(pp::piecewisePoly)
    tiles = collect(keys(pp))
    return makeTree(tiles)
end

function mergeTrees(trees::Vector{treeDict})
    tree = treeDict()
    tiles = StaticRoots()
    while length(tiles) > 0
        for tile in tiles, t in trees
            #add new children
            if haskey(tree,tile) && haskey(t,tile)
                for newtile in t[tile]
                    if newtile ∉ tree[tile]
                        push!(tree[tile],newtile)
                    end
                end
            elseif haskey(t,tile)
                tree[tile] = t[tile]
            end
        end
        # get next level candidates
        # look at children on elements of tiles, over all trees
      @timeit to "reduce 2" begin
        newtiles = Set{DyadicIndexType}()
        for tile in tiles, t in trees
            if haskey(t, tile)
                for el in t[tile]
                    push!(newtiles, el)
                end
            end
        end 
        tiles = newtiles
      end
    end
    return tree
end





