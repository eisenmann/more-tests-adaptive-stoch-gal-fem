# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


# routines for approximate residual evaluation
using SparseArrays
include("MultiIndex.jl")
include("MultiIndexMap.jl")
include("StochOperatorFEM.jl")
include("StochPiecewisePoly.jl")
include("BPX.jl")
struct Summand
    β::Float64
    μ::Int64
    νv::Int64
end

function resApprox(opinfo::OperatorInfo, F::MultiIndexMap, S::Vector{Mesh}, meshdicts::Vector{Matrix{Int64}}, u::FunctionCoeff, ζ::Float64, η0::Real) 
    factor = 2.0^(1/1)
    η = η0*factor
    C = 1
    ell_hat = ceil(Int64,-log2(ζ/C))+1
    ζ_l = C*2.0^(-ell_hat)
    ζeff = (ζ - ζ_l)/ ((1+ζ)*(1+ζ_l))
    
    @printf("   [ entered resApprox: F %d, S %d, ζeff = %.3e\n", length(S), sum(length(s) for s in S), ζeff)

    println("      η\t\trnorm\t\tFext\t\tSext")

    Fext = deepcopy(F)
    BPXcoeffs = Vector{Dict{GlobalFE2D, Float64}}()
    rerrbound = 0.
    norm_res = 0.0
    notdone = true
    applied_approx = DGfunction2D[]
    while notdone
        η /= factor
        Fext,  levelIndices,  ℓ , ηnew = applyStochastic_short(F, S, meshdicts ,u, opinfo, η)
        applied_approx = applySpatial2D(Fext, S, meshdicts, levelIndices,  ℓ ,u, opinfo)
        BPXcoeffs = Dict{GlobalFE2D, Float64}[]
        for r in applied_approx
            push!(BPXcoeffs, Dict{GlobalFE2D, Float64}())
        end
        i_0 = Fext[MultiIndex()]
        n = Threads.nthreads()
        perm = reduce(vcat,i:n:length(applied_approx) for i in 1:n)
    
        Threads.@threads for i = 1:length(applied_approx)
            idx = perm[i]
            BPXcoeffs[idx] = idx == i_0 ? getBPXcoeffs2D(applied_approx[idx], true) : getBPXcoeffs2D(applied_approx[idx]) 
        end
        
        H1dualNorms = H1dualNorm.(BPXcoeffs)
        norm_res = norm(H1dualNorms)
        @printf("     %.3e\t%.3e\t%d\t\t%d\n", η, norm_res, length(Fext), sum(length(p[1]) for p in applied_approx))

        if ηnew ≤ norm_res * ζeff
            notdone = false
            rerrbound = (ηnew + norm_res * ζ_l/(1-ζ_l))
            @printf("   [ exiting resApprox: norm_res = %.3e,  η = %.3e\n", norm_res, ηnew)
        elseif η > norm_res * ζeff * (factor^3)
                η = max(norm_res * ζeff * (factor^3)*0.99, η / (factor^8))
        end
        flush(stdout)
    end

    return Fext, applied_approx ,BPXcoeffs, rerrbound, norm_res
end

function compute_bound(j::Int64, levelIndices::Vector{Vector{Int64}}, levelNorms::Vector{Float64}, opinfo::OperatorInfo, weighted::Float64)
    normB = 1+opinfo.c/(1-0.5^opinfo.α)
    Nj = length(levelIndices[j])
    dj = levelNorms[j]
    d = ( opinfo.μiter == IterHat1D ? 1 : (opinfo.μiter == IterHat2D ? 2 : throw("no dim found") ) )
    return dj==0 ? -1 : ceil(Int64, 1/opinfo.α * log2( ( dj/Nj )^(opinfo.α/(opinfo.α+d)) * weighted ) )
end

function levelSort(S::Vector{Mesh}, meshdicts::Vector{Matrix{Int64}}, u::FunctionCoeff)
    N = length(u)
    J = max(ceil(Int64, log2(N)),1)
    norms = [ normH1(S[i], meshdicts[i], u[i]) for i = 1:N]
    p = sortperm(norms, rev=true)

    levelIndices = Vector{Vector{Int64}}(undef, J)
    if J ≥ 2
        levelIndices[1] = p[1:1]
        for j = 2:J-1
            levelIndices[j] = p[(2^(j-1)):(2^j-1)]
        end
        levelIndices[J] = p[(2^(J-1)):end]
    else
        levelIndices[1] = p[1:end]
    end

    levelNorms = Vector{Float64}(undef, J)
    for j = 1:J
        levelNorms[j] = norm(norms[levelIndices[j]])
    end
    return levelIndices, levelNorms
end


function applyStochastic_short(F::MultiIndexMap, S::Vector{Mesh}, meshdicts::Vector{Matrix{Int64}} ,u::FunctionCoeff, opinfo::OperatorInfo, η::Float64)
    levelIndices, levelNorms = levelSort(S, meshdicts, u)
    J = length(levelIndices)
    normB = 1+opinfo.c/(1-0.5^opinfo.α)
    while normB * norm(levelNorms[J:end]) < η/2
        J -= 1
    end
    δ = normB * norm(levelNorms[J+1:end])
    ℓ = Vector{Int64}(undef, J)
    d = ( opinfo.μiter == IterHat1D ? 1 : (opinfo.μiter == IterHat2D ? 2 : throw("no dim found") ) )
    exp_α_d = (opinfo.α/(opinfo.α+d))
    weighted = opinfo.Capprox/(η-δ) * sum(levelNorms[j]^(d/(opinfo.α+d)) * length(levelIndices[j])^(opinfo.α/(opinfo.α+d)) for j in 1:J )
    for j in 1:J
        ℓ[j] = max(compute_bound(j, levelIndices, levelNorms, opinfo, weighted ) ,-1)
    end
    Fext = deepcopy(F)
    ηnew = (opinfo.Capprox*sum(levelNorms[1:J].*(2.0 .^(-ℓ * opinfo.α )))) + δ
    η = ηnew
    for j in 1:J
        for μ in opinfo.μiter(ℓ[j])
            for νidx in levelIndices[j]
                ν = F[νidx]
                νμ = getValue(ν, μ)
                push!(Fext, ν + MultiIndexBasis(μ))
                if νμ > 0
                    push!(Fext, ν - MultiIndexBasis(μ))
                end
            end
        end
    end
    return Fext,  levelIndices,  ℓ , η
end

# for CG method
function applySpatial2Dgal!(F::MultiIndexMap, S::Vector{Mesh}, meshdicts::Vector{Matrix{Int64}}, level::Int64 ,U::FunctionCoeff, opinfo::OperatorInfo, w::FunctionCoeff, c::Float64)
    PPs = [PiecewisePoly2D() for ν in F]
    coeffs = zeros(length(PPs))
    for (idx,u) in enumerate(U)
        ν = F[idx]
        for l in -1:level
            scale = scaleFactor(opinfo, l)
            indexmap = zeros(Int64, 2 , length(get2DlevelRange(l)))
            if l ≥ 0 
                beforethislevel = (2^(l)-1)^2
                for μ in get2DlevelRange(l)
                    νμ = getValue(ν, μ)
                    if in(F, ν + MultiIndexBasis(μ))
                        idx1 = F[ν + MultiIndexBasis(μ)]
                        indexmap[1, μ - beforethislevel] = idx1
                        coeffs[idx1] = scale * sqrtbeta(νμ + 1)
                    end
                    if νμ > 0
                        if in(F, ν - MultiIndexBasis(μ))
                            idx2 = F[ν - MultiIndexBasis(μ)]
                            indexmap[2, μ - beforethislevel] = idx2
                            coeffs[idx2] = scale * sqrtbeta(νμ)
                        end
                    end
                end
            else
                indexmap[1,1] = idx
                coeffs[idx] = scale
            end
            applyHatLevel!(l, u , PPs , S[idx], meshdicts[idx], indexmap, coeffs)
        end

    end
    @Threads.threads for i in length(w):-1:1
        DGmesh, DGvals = piecewisePolyToDiscontinuousGalerkin(PPs[i])
        
        w[i] += c* discontinuousGalerkinToVector(S[i], meshdicts[i], (DGmesh, DGvals))
    end
    return w
end

# for resApprox 
function applySpatial2D(Fext::MultiIndexMap, S::Vector{Mesh}, meshdicts::Vector{Matrix{Int64}}, levelIndices::Vector{Vector{Int64}},  ℓ::Vector{Int64} ,U::FunctionCoeff, opinfo::OperatorInfo)
    PPs = [PiecewisePoly2D() for ν in Fext]
    coeffs = zeros(length(PPs))
    J = length(ℓ)
    BigJ = length(levelIndices)
    for (level,indices) in zip(ℓ,levelIndices[1:J])
        for idx in indices
            ν = Fext[idx]
            u = U[idx]
            for l in -1:level

                scale = scaleFactor(opinfo, l)
                indexmap = zeros(Int64, 2 , length(get2DlevelRange(l)))
                if l ≥ 0 
                    beforethislevel = (2^(l)-1)^2
                    for μ in get2DlevelRange(l)
                        νμ = getValue(ν, μ)
                        idx1 = Fext[ν + MultiIndexBasis(μ)]
                        indexmap[1, μ - beforethislevel] = idx1
                        coeffs[idx1] = scale * sqrtbeta(νμ + 1)
                        if νμ > 0
                            idx2 = Fext[ν - MultiIndexBasis(μ)]
                            indexmap[2, μ - beforethislevel] = idx2
                            coeffs[idx2] = scale * sqrtbeta(νμ)
                        end
                    end
                else
                    indexmap[1,1] = idx
                    coeffs[idx] = scale
                end
                applyHatLevel!(l, u , PPs , S[idx], meshdicts[idx], indexmap, coeffs)
            end
        end
    end
    DGs = [(Mesh(), BilinearGradInfo[]) for i in 1:length(PPs)]
    @Threads.threads for i in 1:length(PPs)
        DGs[i] = piecewisePolyToDiscontinuousGalerkin(PPs[i])
    end
    return DGs
end

# End 2D
