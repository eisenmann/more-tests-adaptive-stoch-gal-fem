# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


# given by smalles dyadic square that contains the triangle and a number that defines which one it is from 0 to 5

#=
Even case
------- 
|\  0 |
| \   |
|  \  |
| 1 \ |
-------

-------
|\ 2/ |
| \/ 4|
|3/\  |
|/5 \ |
-------

odd case
------- 
| 0 / |
|  /  |
| /   |
|/ 1  |
-------

-------
|\ 2/ |
| \/ 3|
|4/\  |
|/5 \ |
-------
=#

SquareIndex2D = Tuple{Int64,Int64,Int64}
TriangleIndex2D = Tuple{Int64,Int64,Int64,Int64} #level of square, horizontal index of square, vertical index of square, number in square
EdgeIndex2D = Tuple{Int64,Int64,Int64,Int64} 
#level of square, horizontal index of square, 
#vertical index of square, 
#type: horizontal:0, vertical:1, diagonal:2
VertexIndex2D = Tuple{Int64,Int64,Int64} #level of square, horizontal index of square, vertical index of square
const Mesh = Vector{TriangleIndex2D}
const GlobalFE2D = Tuple{Int64,Int64,Int64}


if LSHAPED == true
    println("L Shaped domain (Tree2D)")
    #-----------Domain def for L shape
    function StaticRoots()
        return [ (1, 0, 0, 0), (1, 1, 0, 0), (1, 1, 0, 1), (1, 0, 0, 1), (1, 1, 1, 0), (1, 1, 1, 1)]
    end


    function onBoundary(v::VertexIndex2D)
        return v[2]==0 || v[3]==0 || v[2]==2^v[1] || v[3]==2^v[1] || ( v[2]==2^(v[1]-1) && v[3] ≥ 2^(v[1]-1) ) || ( v[3]==2^(v[1]-1) && v[2] ≤ 2^(v[1]-1) )
    end
    
    function inDomain(v::VertexIndex2D)
        return !onBoundary(v)
    end

    function edgeOnBoundary(e::EdgeIndex2D)
        a,b = endVertexes(e)
        return onBoundary(a) && onBoundary(b) && !isDiag(e)
    end
    #------------end domain def for L shape
else

    println("Square domain (Tree2D)")
    #-----------Domain def for unit square
    function StaticRoots()
        return [(0,0,0,1),(0,0,0,0)]
    end

    function onBoundary(v::VertexIndex2D)
        return v[2]==0 || v[3]==0 || v[2]==2^v[1] || v[3]==2^v[1]
    end

    function inDomain(v::VertexIndex2D)
#        @assert !onBoundary(v) == v[1] != 0 && v[2] != 0 && v[3] != 0 && v[2]!= 2^v[1] && v[3]!= 2^v[1]
        return !onBoundary(v)
    end

    function edgeOnBoundary(e::EdgeIndex2D)
        a,b = endVertexes(e)
        return onBoundary(a) && onBoundary(b) && !isDiag(e)
    end
    #------------end domain def for unit square
end

function sublevel(tile::TriangleIndex2D)
    (tile[4]>1)
end
function iseven(tile::TriangleIndex2D)
    return (tile[2]+tile[3]) % 2 == 0
end
function iseven(tile::VertexIndex2D)
    return (tile[2]+tile[3]) % 2 == 0
end

function level(tile::TriangleIndex2D)
    return 2*tile[1]+sublevel(tile)
end

function j(tile::TriangleIndex2D)
    return level(tile)
end


function intersect(tile1::TriangleIndex2D, tile2::TriangleIndex2D)
    l = min(tile1[1], tile2[1])
    l1 = tile1[1] - l
    l2 = tile2[1] - l
    if tile1[2] ÷ 2^l1 == tile2[2] ÷ 2^l2 && tile2[3] ÷ 2^l2 == tile1[3] ÷ 2^l1
        leveldiff  = 2*(tile1[1] - tile2[1]) + (tile1[4]>1) - (tile2[4]>1)
        if leveldiff > 0
            return isInside(center(tile1),tile2) 
        else
            return isInside(center(tile2),tile1)
        end
    else
        return false
    end
end
function longEdge(idx::TriangleIndex2D) 
    if sublevel(idx)
        #Even case
        if iseven(idx)
            if idx[4] == 2
                return (idx[1],idx[2],idx[3],0)
            elseif idx[4] == 3
                return (idx[1],idx[2],idx[3],1)
            elseif idx[4] == 4
                return (idx[1],idx[2]+1,idx[3],1)
            else #idx[4] == 5
                return (idx[1],idx[2],idx[3]+1,0)
            end
        else
        #Odd case
            if idx[4] == 2
                return (idx[1],idx[2],idx[3],0)
            elseif idx[4] == 3
                return (idx[1],idx[2]+1,idx[3],1)
            elseif idx[4] == 4
                return (idx[1],idx[2],idx[3],1)
            else #idx[4] == 5
                return (idx[1],idx[2],idx[3]+1,0)
            end
        end
    else
        return (idx[1],idx[2],idx[3],2)
    end
end

function shortEdges(idx::TriangleIndex2D) 
    #Even case
    if iseven(idx)
        if idx[4] == 0
            return [(idx[1],idx[2],idx[3],0), (idx[1],idx[2]+1,idx[3],1)]
        elseif idx[4] == 1
            return [(idx[1],idx[2],idx[3],1), (idx[1],idx[2],idx[3]+1,0)]
        elseif idx[4] == 2
            return [(idx[1]+1,2*idx[2],2*idx[3],2), (idx[1]+1,2*idx[2]+1,2*idx[3],2)]
        elseif idx[4] == 3
            return [(idx[1]+1,2*idx[2],2*idx[3],2), (idx[1]+1,2*idx[2],2*idx[3]+1,2)]
        elseif idx[4] == 4
            return [(idx[1]+1,2*idx[2]+1,2*idx[3],2), (idx[1]+1,2*idx[2]+1,2*idx[3]+1,2)]
        else #idx[4] == 5
            return [(idx[1]+1,2*idx[2],2*idx[3]+1,2), (idx[1]+1,2*idx[2]+1,2*idx[3]+1,2)]
        end
    else
    #Odd case
        if idx[4] == 0
            return [(idx[1],idx[2],idx[3],0), (idx[1],idx[2],idx[3],1)]
        elseif idx[4] == 1
            return [(idx[1],idx[2]+1,idx[3],1), (idx[1],idx[2],idx[3]+1,0)]
        elseif idx[4] == 2
            return [(idx[1]+1,2*idx[2],2*idx[3],2), (idx[1]+1,2*idx[2]+1,2*idx[3],2)]
        elseif idx[4] == 3
            return [(idx[1]+1,2*idx[2]+1,2*idx[3],2), (idx[1]+1,2*idx[2]+1,2*idx[3]+1,2)]
        elseif idx[4] == 4
            return [(idx[1]+1,2*idx[2],2*idx[3],2), (idx[1]+1,2*idx[2],2*idx[3]+1,2)]
        else #idx[4] == 5
            return [(idx[1]+1,2*idx[2],2*idx[3]+1,2), (idx[1]+1,2*idx[2]+1,2*idx[3]+1,2)]
        end
    end
end

function innerEdges(t::TriangleIndex2D)
    edges = [longEdge(t),shortEdges(t)...]
    return [e for e in edges if !edgeOnBoundary(e)]
end
function isDiag(edge::EdgeIndex2D)
    return edge[4] == 2
end

function squarechildren(idx::SquareIndex2D) 
    return ((idx[1]+1,2*idx[2],2*idx[3]),(idx[1]+1,2*idx[2]+1,2*idx[3]), 
        (idx[1]+1,2*idx[2]+1,2*idx[3]+1), (idx[1]+1,2*idx[2],2*idx[3]+1))
end


#return the using propper tree order
function children(idx::TriangleIndex2D) 
    return collect(childrenTuple(idx))
end

function grandchildren(idx::TriangleIndex2D)
    if iseven(idx)
        return ((idx[1]+1, 2*idx[2], 2*idx[3], idx[4]), (idx[1]+1, 2*idx[2]+(idx[4]==0), 2*idx[3]+(idx[4]==1), 0), (idx[1]+1, 2*idx[2]+(idx[4]==0), 2*idx[3]+(idx[4]==1), 1), (idx[1]+1, 2*idx[2]+1, 2*idx[3]+1, idx[4]) )
    else
        return ((idx[1]+1, 2*idx[2]+(idx[4]==1), 2*idx[3]+(idx[4]==1), 0), (idx[1]+1, 2*idx[2]+(idx[4]==1), 2*idx[3]+(idx[4]==1), 1), (idx[1]+1, 2*idx[2]+1, 2*idx[3], idx[4]), (idx[1]+1, 2*idx[2], 2*idx[3]+1, idx[4]))
    end
end

function grandancestor(idx::TriangleIndex2D)
    @assert idx[4] < 2

    if iseven((idx[1]-1,idx[2]÷2,idx[3]÷2, 0))
        if idx[2] % 2 == idx[3] % 2
            return (idx[1]-1,idx[2]÷2,idx[3]÷2, idx[4])
        else
            return (idx[1]-1,idx[2]÷2,idx[3]÷2, idx[3] % 2 )
        end
    else #odd
        if idx[2] % 2 ≠ idx[3] % 2
            return (idx[1]-1,idx[2]÷2,idx[3]÷2, idx[4])
        else
            return (idx[1]-1,idx[2]÷2,idx[3]÷2, idx[3] % 2 )
        end
    end
end

function childrenTuple(idx::TriangleIndex2D) 
    if sublevel(idx)
        #Even case
        if iseven(idx)
            if idx[4] == 2
                return ((idx[1]+1,2*idx[2],2*idx[3],0), (idx[1]+1,2*idx[2]+1,2*idx[3],0))
            elseif idx[4] == 3
                return ((idx[1]+1,2*idx[2],2*idx[3],1), (idx[1]+1,2*idx[2],2*idx[3]+1,0))
            elseif idx[4] == 4
                return ((idx[1]+1,2*idx[2]+1,2*idx[3],1), (idx[1]+1,2*idx[2]+1,2*idx[3]+1,0))
            else #idx[4] == 5
                return ((idx[1]+1,2*idx[2],2*idx[3]+1,1), (idx[1]+1,2*idx[2]+1,2*idx[3]+1,1))
            end
        else
        #Odd case
            if idx[4] == 2
                return ((idx[1]+1,2*idx[2],2*idx[3],0), (idx[1]+1,2*idx[2]+1,2*idx[3],0))
            elseif idx[4] == 3
                return ((idx[1]+1,2*idx[2]+1,2*idx[3],1), (idx[1]+1,2*idx[2]+1,2*idx[3]+1,0))
            elseif idx[4] == 4
                return ((idx[1]+1,2*idx[2],2*idx[3],1), (idx[1]+1,2*idx[2],2*idx[3]+1,0))
            else #idx[4] == 5
                return ((idx[1]+1,2*idx[2],2*idx[3]+1,1), (idx[1]+1,2*idx[2]+1,2*idx[3]+1,1))
            end
        end
    else
        return ((idx[1], idx[2], idx[3], idx[4]+2 ), (idx[1], idx[2], idx[3], idx[4]+4 ) )
    end
end

function children(idx::TriangleIndex2D, l::Integer) 
    if l == 1
        return children(idx)
    else
        return reduce(vcat, children.(children(idx,l-1)) ) 
    end
end

function vertexes(idx::TriangleIndex2D)
    #Even case
    if iseven(idx)
        if idx[4] == 0
            return ((idx[1],idx[2]+1,idx[3]), (idx[1],idx[2]+1,idx[3]+1), (idx[1],idx[2],idx[3]))
        elseif idx[4] == 1
            return ((idx[1],idx[2],idx[3]+1), (idx[1],idx[2],idx[3]), (idx[1],idx[2]+1,idx[3]+1))
        elseif idx[4] == 2
            return ((idx[1]+1,2*idx[2]+1,2*idx[3]+1), (idx[1],idx[2],idx[3]), (idx[1],idx[2]+1,idx[3]))
        elseif idx[4] == 3
            return ((idx[1]+1,2*idx[2]+1,2*idx[3]+1), (idx[1],idx[2],idx[3]+1), (idx[1],idx[2],idx[3]))
        elseif idx[4] == 4
            return ((idx[1]+1,2*idx[2]+1,2*idx[3]+1), (idx[1],idx[2]+1,idx[3]), (idx[1],idx[2]+1,idx[3]+1))
        else #idx[4] == 5
            return ((idx[1]+1,2*idx[2]+1,2*idx[3]+1), (idx[1],idx[2]+1,idx[3]+1), (idx[1],idx[2],idx[3]+1))
        end
    else
    #Odd case
        if idx[4] == 0
            return ((idx[1],idx[2],idx[3]), (idx[1],idx[2]+1,idx[3]), (idx[1],idx[2],idx[3]+1))
        elseif idx[4] == 1
            return ((idx[1],idx[2]+1,idx[3]+1), (idx[1],idx[2],idx[3]+1), (idx[1],idx[2]+1,idx[3]))
        elseif idx[4] == 2
            return ((idx[1]+1,2*idx[2]+1,2*idx[3]+1), (idx[1],idx[2],idx[3]), (idx[1],idx[2]+1,idx[3]))
        elseif idx[4] == 3
            return ((idx[1]+1,2*idx[2]+1,2*idx[3]+1), (idx[1],idx[2]+1,idx[3]), (idx[1],idx[2]+1,idx[3]+1))
        elseif idx[4] == 4
            return ((idx[1]+1,2*idx[2]+1,2*idx[3]+1), (idx[1],idx[2],idx[3]+1), (idx[1],idx[2],idx[3]))
        else #idx[4] == 5
            return ((idx[1]+1,2*idx[2]+1,2*idx[3]+1), (idx[1],idx[2]+1,idx[3]+1), (idx[1],idx[2],idx[3]+1))
        end
    end
end


function diagColorVertex(idx::TriangleIndex2D)
    #Even case
    if iseven(idx)
        if idx[4] == 0
            return (idx[1],idx[2]+1,idx[3])
        elseif idx[4] == 1
            return (idx[1],idx[2],idx[3]+1)
        elseif idx[4] == 2
            return (idx[1]+1,2*idx[2]+1,2*idx[3])
        elseif idx[4] == 3
            return (idx[1]+1,2*idx[2],2*idx[3]+1)
        elseif idx[4] == 4
            return (idx[1]+1,2*idx[2]+2,2*idx[3]+1)
        else #idx[4] == 5
            return (idx[1]+1,2*idx[2]+1,2*idx[3]+2)
        end
    else
    #Odd case
        if idx[4] == 0
            return (idx[1],idx[2],idx[3])
        elseif idx[4] == 1
            return (idx[1],idx[2]+1,idx[3]+1)
        elseif idx[4] == 2
            return (idx[1]+1,2*idx[2]+1,2*idx[3])
        elseif idx[4] == 3
            return (idx[1]+1,2*idx[2]+2,2*idx[3]+1)
        elseif idx[4] == 4
            return (idx[1]+1,2*idx[2],2*idx[3]+1)
        else #idx[4] == 5
            return (idx[1]+1,2*idx[2]+1,2*idx[3]+2)
        end
    end
end

function isDiagEven(idx::TriangleIndex2D)
    #Even case
    if iseven(idx)
        if idx[4] == 0
            return (idx[2]+1) % 2 == 0
        elseif idx[4] == 1
            return idx[2] % 2 == 0
        elseif idx[4] == 2
            return (2*idx[2]+1) % 2 == 0
        elseif idx[4] == 3
            return (2*idx[2]) % 2 == 0
        elseif idx[4] == 4
            return (2*idx[2]+2) % 2 == 0
        else #idx[4] == 5
            return (2*idx[2]+1) % 2 == 0
        end
    else
    #Odd case
        if idx[4] == 0
            return idx[2] % 2 == 0
        elseif idx[4] == 1
            return (idx[2]+1) % 2 == 0
        elseif idx[4] == 2
            return (2*idx[2]+1) % 2 == 0
        elseif idx[4] == 3
            return (2*idx[2]+2) % 2 == 0
        elseif idx[4] == 4
            return (2*idx[2]) % 2 == 0
        else #idx[4] == 5
            return (2*idx[2]+1) % 2 == 0
        end
    end
end

function endVertexes(idx::EdgeIndex2D)
    if idx[4] == 0
        return ((idx[1],idx[2],idx[3]), (idx[1],idx[2]+1,idx[3]))
    elseif idx[4] == 1
        return ((idx[1],idx[2],idx[3]), (idx[1],idx[2],idx[3]+1))
    elseif idx[4] == 2
        if iseven(idx)
            return ((idx[1],idx[2],idx[3]), (idx[1],idx[2]+1,idx[3]+1))
        else
            return ((idx[1],idx[2],idx[3]+1), (idx[1],idx[2]+1,idx[3]))
        end
    end
end


function center(idx::TriangleIndex2D)
    #Even case
    if iseven(idx)
        if idx[4] == 0
            return (idx[1]+2,idx[2]*4+3,idx[3]*4+1)
        elseif idx[4] == 1
            return (idx[1]+2,idx[2]*4+1,idx[3]*4+3)
        elseif idx[4] == 2
            return (idx[1]+2,idx[2]*4+2,idx[3]*4+1)
        elseif idx[4] == 3
            return (idx[1]+2,idx[2]*4+1,idx[3]*4+2)
        elseif idx[4] == 4
            return (idx[1]+2,idx[2]*4+3,idx[3]*4+2)
        else #idx[4] == 5
            return (idx[1]+2,idx[2]*4+2,idx[3]*4+3)
        end
    else
    #Odd case
        if idx[4] == 0
            return (idx[1]+2,idx[2]*4+1,idx[3]*4+1)
        elseif idx[4] == 1
            return (idx[1]+2,idx[2]*4+3,idx[3]*4+3)
        elseif idx[4] == 2
            return (idx[1]+2,idx[2]*4+2,idx[3]*4+1)
        elseif idx[4] == 3
            return (idx[1]+2,idx[2]*4+3,idx[3]*4+2)
        elseif idx[4] == 4
            return (idx[1]+2,idx[2]*4+1,idx[3]*4+2)
        else #idx[4] == 5
            return (idx[1]+2,idx[2]*4+2,idx[3]*4+3)
        end
    end
end
function coords(v::VertexIndex2D)
    (0,1) .+ 2.0^(-v[1]).*(v[2],-v[3])
end
function squareCoords(v::SquareIndex2D)
    return ((0,1) .+ 2.0^(-v[1]).*(v[2],-v[3]),(0,1) .+ 2.0^(-v[1]).*(v[2]+1,-v[3]),
        (0,1) .+ 2.0^(-v[1]).*(v[2]+1,-v[3]-1), (0,1) .+ 2.0^(-v[1]).*(v[2],-v[3]-1))

end
function bary(tile::TriangleIndex2D, x::NTuple{2, Real})
	v1,v2,v3 = coords.(vertexes(tile))
	w = x .- v1
	w2 = v2 .- v1
	w3 = v3 .- v1
    scale = 2^(2*tile[1]+(tile[4]>1))
	λ2 = scale * (w[1]*w2[1] + w[2]*w2[2])
	λ3 = scale * (w[1]*w3[1] + w[2]*w3[2])
    λ1 = 1 - λ2 - λ3
    return (λ1 , λ2 , λ3) 
end

function bary(pts::NTuple{3,NTuple{2,Float64}}, x::NTuple{2,Float64})
    A = [1 1 1; pts[1][1] pts[2][1] pts[3][1]; pts[1][2] pts[2][2] pts[3][2]]
    return A\[1,x[1],x[2]]
end
function isInside(v::VertexIndex2D,t::TriangleIndex2D)
    b = bary(t, coords(v))
    return b[1] > 0 && b[2]>0 && b[3]>0
end

function ancestor_safe(idx::TriangleIndex2D, l::Integer=1)
    t = ancestor(idx,l)
    @assert isInside(center(idx), t)
    return t
end

function ancestor!(idx::TriangleIndex2D)
    if idx[4]>1
        idx[4] = idx[4] & 1
    else
        if isodd(idx[2]) 
            #right
            if isodd(idx[3])
                idx[1] -= 1
                idx[2] = idx[2] ÷ 2
                idx[3] = idx[3] ÷ 2
                #bottom right
                if idx[4] == 0
                    if iseven(idx)
                        idx[4] = 4
                    else
                        idx[4] = 3
                    end
                else #idx[4] == 1
                    idx[4] = 5
                end
            else
                idx[1] -= 1
                idx[2] = idx[2] ÷ 2
                idx[3] = idx[3] ÷ 2
                #top right
                if idx[4] == 0
                    idx[4] = 2
                else #idx[4] == 1
                    if iseven(pt)
                        idx[4] = 4
                    else
                        idx[4] = 3
                    end
                end
            end
        else
            #left
            if isodd(idx[3])
                idx[1] -= 1
                idx[2] = idx[2] ÷ 2
                idx[3] = idx[3] ÷ 2
                #bottom left
                if idx[4] == 0
                    if iseven(idx)
                        idx[4] = 3
                    else
                        idx[4] = 4
                    end
                else #idx[4] == 1
                    idx[4] = 5
                end
            else
                idx[1] -= 1
                idx[2] = idx[2] ÷ 2
                idx[3] = idx[3] ÷ 2
                #top left
                if idx[4] == 0
                    idx[4] = 2
                else #idx[4] == 1
                    if iseven(pt)
                        idx[4] = 3
                    else
                        idx[4] = 4
                    end
                end
            end
        end         
    end
    return idx
end

function ancestor(idx::TriangleIndex2D, l::Integer=1)
    if l == 0
        return idx
    elseif l == 1
        if idx[4]>1
            return (idx[1],idx[2],idx[3],idx[4]&1)
        else
            pt = (idx[1] - 1, idx[2] ÷ 2, idx[3] ÷ 2) #parent tile
            if isodd(idx[2]) 
                #right
                if isodd(idx[3])
                    #bottom right
                    if idx[4] == 0
                        if iseven(pt)
                            return (pt..., 4)
                        else
                            return (pt..., 3)
                        end
                    else #idx[4] == 1
                        return (pt..., 5)
                    end
                else
                    #top right
                    if idx[4] == 0
                        return (pt..., 2)
                    else #idx[4] == 1
                        if iseven(pt)
                            return (pt..., 4)
                        else
                            return (pt..., 3)
                        end
                    end
                end
            else
                #left
                if isodd(idx[3])
                    #bottom left
                    if idx[4] == 0
                        if iseven(pt)
                            return (pt..., 3)
                        else
                            return (pt..., 4)
                        end
                    else #idx[4] == 1
                        return (pt..., 5)
                    end
                else
                    #top left
                    if idx[4] == 0
                        return (pt..., 2)
                    else #idx[4] == 1
                        if iseven(pt)
                            return (pt..., 3)
                        else
                            return (pt..., 4)
                        end
                    end
                end
            end
                
        end
    else
        if idx[4] ≤ 1
            diadicl = (l ÷ 2)
            t = (idx[1] - diadicl, idx[2] ÷ 2^diadicl, idx[3] ÷ 2^diadicl, 0)
            if !isInside(center(idx), t)
                t=(t[1:3]...,1)
            end
            return ancestor(t,l-2*diadicl)
        else
            return ancestor(ancestor(idx), l - 1)
        end
    end
end

function checkDomain(t::TriangleIndex2D)
    return true#intersect(StaticRoots(),t)
end
function inDomain(t::Vector{TriangleIndex2D})
    flags = checkDomain.(t)
    return t[flags]
end
function largeTriangles(idx::EdgeIndex2D)
    if idx[4]==0 #horizontal edge 
        if iseven(idx)
            return inDomain([(idx[1],idx[2],idx[3]-1,1),(idx[1],idx[2],idx[3],0)])
        else
            return inDomain([(idx[1],idx[2],idx[3]-1,1),(idx[1],idx[2],idx[3],0)])
        end
    elseif idx[4]==1 #vertical edge
        if iseven(idx)
            return inDomain([(idx[1],idx[2]-1,idx[3],1),(idx[1],idx[2],idx[3],1)])
        else
            return inDomain([(idx[1],idx[2]-1,idx[3],0),(idx[1],idx[2],idx[3],0)])
        end
    else idx[4]==2 #diagonal edge
        return inDomain([(idx[1:3]...,0),(idx[1:3]...,1)])
    end
end
function smallTriangles(idx::EdgeIndex2D)
    if idx[4]==0 #horizontal edge 
        if iseven(idx)
            return inDomain([(idx[1],idx[2],idx[3]-1,5),(idx[1],idx[2],idx[3],2)])
        else
            return inDomain([(idx[1],idx[2],idx[3]-1,5),(idx[1],idx[2],idx[3],2)])
        end
    elseif idx[4]==1 #vertical edge
        if iseven(idx)
            return inDomain([(idx[1],idx[2]-1,idx[3],3),(idx[1],idx[2],idx[3],3)])
        else
            return inDomain([(idx[1],idx[2]-1,idx[3],4),(idx[1],idx[2],idx[3],4)])
        end
    else idx[4]==2 #diagonal edge
        return Vector{TriangleIndex2D}()
    end
end


function simplifyVertex(node::VertexIndex2D)
    while node[2] % 2 == 0 && node[3] % 2 == 0 && node[1]>0
        node = (node[1]-1, node[2] ÷ 2, node[3] ÷ 2)
    end
    return node
end
#largest possible triangles with corners in v
#vertex should be simplified
function getVertexTriangles(v::VertexIndex2D)
    if iseven(v)
        #if this should be applied to non simplified vertices,
        # then replace'÷' by 'Int(ceil( /2))-1' I guess
        return (
            (v[1] - 1, v[2] ÷ 2, v[3] ÷ 2, 2),
            (v[1] - 1, v[2] ÷ 2, v[3] ÷ 2, 3),
            (v[1] - 1, v[2] ÷ 2, v[3] ÷ 2, 4),
            (v[1] - 1, v[2] ÷ 2, v[3] ÷ 2, 5)
        )
    else
        return (
            (v[1], v[2] - 1 , v[3] - 1 , 1),
            (v[1], v[2]     , v[3] - 1 , 1),
            (v[1], v[2]     , v[3]     , 0),
            (v[1], v[2] - 1 , v[3]     , 0)
        )
    end
end
#largest possible squares with corners in v
#vertex should be simplified
function getVertexSquares(v::VertexIndex2D)
    return (
            (v[1], v[2] - 1 , v[3] - 1 ),
            (v[1], v[2]     , v[3] - 1 ),
            (v[1], v[2]     , v[3]     ),
            (v[1], v[2] - 1 , v[3]     )
        )
end

function getSquareVertices(v::VertexIndex2D)
    return (
            (v[1], v[2]     , v[3]     ),
            (v[1], v[2] + 1 , v[3]     ),
            (v[1], v[2] + 1 , v[3] + 1 ),
            (v[1], v[2]     , v[3] + 1 )
        )
end

#vertex should be simplified, otherwise meaningless
function getVertexParents(v::VertexIndex2D)
    p = endVertexes(longEdge(ancestor(getVertexTriangles(v)[1])))
    return p[1], p[2]
end

function area(tile::TriangleIndex2D)
    if tile[4]>1
        return 4.0^(-tile[1]-1)
    else
        return 2.0^(-2*tile[1]-1)
    end
end

function quadraturePoints(tile::TriangleIndex2D)
    v1,v2,v3 = coords.(vertexes(tile))
    return (v2.+v3)./2, (v3.+v1)./2, (v1.+v2)./2
end

VertexIndexType = VertexIndex2D
DyadicIndexType = TriangleIndex2D
include("Tree.jl")