# Julia implementation of an adaptive stochastic Galerkin finite element method with multilevel representations of random coefficients
# Released under MIT License, see README.md
# Copyright (c) 2024 Markus Bachmayr, Henrik Eisenmann, Igor Voulis


using Plots
using LegendrePolynomials
using LaTeXStrings

function plotTriangle!(t::TriangleIndex2D)
    c = coords.(vertexes(t))
    x = [x for (x,y) in c]
    y = [y for (x,y) in c]
    plot!(x, y, seriestype=:shape, fillalpha=0.1, legend = false)
end

function plotEdge!(e::EdgeIndex2D)
    c = coords.(endVertexes(e))
    x = [x for (x,y) in c]
    y = [y for (x,y) in c]
    plot!(x, y, legend = false)
end

function scatterVertex!(v::VertexIndex2D)
    scatter!(coords(v))
end

function plotMesh(triangles::Vector{TriangleIndex2D})
    plot(    xlims = (0,1) , ylims=(0,1))
    for t in triangles
        plotTriangle!(t)
        for e in [longEdge(t),shortEdges(t)...]
            plotEdge!(e)
        end

    end
    plot!()
end

function piecewiseconstplot(mesh::Mesh,s::Vector{Float64})
    p = plot(aspect_ratio = 1, framestyle = :box, grid = :none, legend =:none, xlims = (0,1) , ylims=(0,1))

    for (triangle,value) in zip(mesh,s)
        data = collect(coords.(vertexes(triangle)))
        p = plot!(Shape(data), fill_z = value, linewidth = 1, linealpha = 0.3, line_z = value)
    end
    return p
end

function averageValueOnTriangle(mesh::Mesh, meshdict::Matrix{Int64}, u::Vector{Float64})
    s_triangles = zeros(length(mesh))
    for n in 1:length(mesh)
        s = 0.
        for j in 1:3
            s += meshdict[j,n]==0 ? 0.0 : u[meshdict[j,n]]
        end
        s_triangles[n] = 1/3*s
    end
    return s_triangles
end


function piecewiselinearData(mesh::Mesh, meshdict::Matrix{Int64}, u::Vector{Float64}, resolution = 10)
    N = 2^resolution + 1
    data = zeros(N,N)
    meshr = reduce(vcat, children.(StaticRoots(), 2 * (resolution - LSHAPED)))
    meshdictr = meshDict(meshr)
    ur =  inclusion(mesh, meshr, meshdict, meshdictr, u )

    for (n,tile) in enumerate(meshr)
        for (j,vertex) in enumerate(vertexes(tile))
            if meshdictr[j,n]!=0 
                s = ur[meshdictr[j,n]]
                data[N - vertex[3] , vertex[2]+1] = s
            end
        end
    end
    return data
end


function plotlines!(mesh::Mesh, width = 0.1)
    data = Tuple{Float64,Float64}[]
    for triangle in mesh
        append!(data,collect(coords.(vertexes(triangle))))
        push!(data, coords.(vertexes(triangle))[1])
        push!(data, (NaN, NaN))
    end
    
    plot!(data, color = :black, label = :none, linewidth=width)
end


function plotlines(mesh::Mesh)
    plot(aspect_ratio = 1,  grid = :none, xlims = (0,1) , ylims=(0,1))
    plotlines!(mesh)
end

function piecewiselinearplot(mesh::Mesh, u::Vector{Float64}; resolution = 10, clr = :RdBu_8, colorbaropt = false)
    meshdict = meshDict(mesh)
    N = 2^resolution + 1
    data = piecewiselinearData(mesh, meshdict, u, resolution)
    x = range(0,1,N)
    y = range(0,1,N)
    l = maximum(abs.(data))
    heatmap(x,y,data,aspect_ratio = 1, framestyle = :none, grid = :none, xlims = (0,1) , ylims=(0,1), c=clr, clim=(-l,l), legend = colorbaropt)
    #L Shape 
    rectangle(w, h, x, y) = Shape(x .+ [0,w,w,0], y .+ [0,0,h,h])

    if LSHAPED
        plot!(rectangle(0.5,0.5,0,0),linewidth = 0, color = :white ,legend = colorbaropt)
    end
    return plotlines!(mesh,0.1)
end

function sum_u(weights::Vector{Float64}, u::Vector{Vector{Float64}}, S::Vector{Vector{DyadicIndexType}})
    meshdicts = meshDict.(S)
    allTiles = vcat(S...)
    finemesh = getMesh(makeMinTree(allTiles))
    finemesh = removeHangingNodes(finemesh)
    finemeshdict = meshDict(finemesh)
    ur = sum(inclusion(S[i], finemesh, meshdicts[i], finemeshdict, weights[i]*u[i]) for i in eachindex(S))
    return ur, finemesh
end

function max_dim(F::MultiIndexMap)
    return max((dim(F[i]) for i in 1:length(F))...)
end

function legendrePoly(ν::MultiIndex)
    y -> prod( Pl(y[μ],getValue(ν,μ))*sqrt(getValue(ν,μ)*2+1) for μ in 1:length(y) )
end

function stocheval(F,S,u,y)
    u_in_y, mesh = sum_u([legendrePoly(F[i])(y) for i in 1:length(F)], u, S)
    return u_in_y, mesh
end

function conv_plot2d(timed, α, offset = 2)
    data = transpose(reshape( [timed[i][j] for i in eachindex(timed) for j in 1:4], 4, length(timed) ))
    plot(framestyle =:box)
    plot!(max.(data[offset:end,2],(1)), data[offset:end,1], label = "time", xscale=:log10, yscale=:log10, linestyle =:dashdot)
    plot!(data[offset:end,3], data[offset:end,1], label = L"\# F")
    plot!(data[offset:end,4], data[offset:end,1], label = L"\mathcal{N}(\mathbb{T})")
    
    minerr = minimum(data[offset:end,1])
    maxerr = maximum(data[offset:end,1])
    maxax = maximum(data[offset:end,4])
    rate = α/2
    f(x) = x^(-rate) *  minerr*maxax^rate 
    plot!(f, label=latexstring(L"rate $s=",round.(rate, digits = 3) , L"$"), xscale=:log10, yscale=:log10,linestyle=:dash)
    plot!(ylims = (minerr, maxerr), xlims =(1, maxax), xticks = 10.0.^(0:floor(log10(maxax))))
    return plot!(title = latexstring("d = 2, \\alpha = $(round(α, digits = 3))"))
end

function MonteCarloEstimator(F::MultiIndexMap, S::Vector{Mesh}, u::FunctionCoeff, α::Float64; samples::Int64 = 100, extra_refinements::Int64 = 1, tolerance::Float64 = 1e-10, level::Int64 =  7)
    errors = Float64[]
    for it in 1:samples
        y = 2*rand((2^level-1)^2) .- 1
        u_in_y, mesh = stocheval(F,S,u,y)
        finemesh = children(mesh, 2*extra_refinements)
        u_y_ref = PointwiseSolver2D(α, finemesh, [y], zeros([finemesh]), tolerance)[1]
        diff_sol, mesh_d = sum_u([1.0,-1.0],[u_y_ref,u_in_y],[finemesh, mesh])
        push!(errors,normH1(mesh_d, meshDict(mesh_d), diff_sol))
        currEst = (norm(errors)/sqrt(it))
        currVar = 1
        
        if it >1
            currVar = norm(errors.^2 .- currEst^2)/sqrt(it-1)
        end
        println("Estimated error after $it samples is $currEst,  Control number is $currVar")
    end
    
    return norm(errors)/sqrt(samples), errors
end